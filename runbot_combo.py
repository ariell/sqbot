import os
import time
import traceback
from runbot import get_annotation_set_query, get_cfg, get_bot, create_parser
from sqapi.api import SQAPI


parser = create_parser()
args = vars(parser.parse_args())   # get args as dict
maincfg = get_cfg(args.cfg)
sqapi = SQAPI()

# Configure bot params from cfg file and override with CLI args if provided
annotation_set_parameters = maincfg.get('annotation_set_parameters')
annotation_set_parameters.update({k: v for k, v in vars(args).items() if v is not None})
annotation_set_query = get_annotation_set_query(sqapi, annotation_set_parameters)


# get a list of models
models = []
for botcfg in maincfg.get("botcfgs"):
    print(f"Loading bot for '{botcfg}'")
    models.append(get_bot(get_cfg(botcfg), os.path.dirname(botcfg), host=sqapi.host, api_key=sqapi.api_key))

# Get annotation_sets
while True:
    annotation_sets = annotation_set_query.execute().json()
    for a in annotation_sets.get("objects"):
        emailmsg = f'Hi {a.get("user", {}).get("first_name")}, <br><br>\n' \
                f"Your dataset, '{a.get('name')}' ({a.get('id')}), has been processed by {len(models)} models: <br><br>\n"
        # run all models
        for m in models:
            try:
                print(f"\nProcessing '{a.get('name')}' ({a.get('id')}) with '{m.annotator_info}'...\n")
                media_count, point_count, annotation_count = m.process_annotation_set(a)
                emailmsg += f" - {m.annotator_info} assessed {point_count} points and {annotation_count} annotations.<br>\n"
            except Exception as e:
                emailmsg += f" - {m.annotator_info} - ERROR: {e}.<br>\n"
                print(traceback.format_exc())

        if maincfg.get('email_results', True):
            annotation_set_url = "{}/geodata/annotation_set/{}".format(sqapi.host, a.get("id"))
            emailmsg += f'<br>To see results, click: <a href="{annotation_set_url}">{annotation_set_url}</a>'
            sqapi.send_user_email("SQ+ BOT: your Annotation Set has been processed", emailmsg, user_ids=[a.get('user', {}).get('id')])
    else:
        print("No annotation sets to process...")
    if isinstance(maincfg.get("poll_delay"), (float, int)) and maincfg.get("poll_delay") > 0:
        time.sleep(maincfg.get("poll_delay"))
    else:
        break

