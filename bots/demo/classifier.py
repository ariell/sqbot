import random
from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf
from sqapi.helpers import cli_init, create_parser
from sqapi.media import SQMediaObject


class RandoBOT(Annotator):
    def __init__(self, **annotator_args):
        """
        RandoBOT automated annotator
        An example of an automated labelling bot that selects random class labels to assign to points.
        It provides terrible suggestions, however it provides a simple boiler-plate example of how to integrate a
        Machine Learning algorithm for label suggestions.
        """
        super().__init__(**annotator_args)
        self.possible_codes = ["ECK", "ASC", "SUB"]

    def classify_point(self, mediaobj: SQMediaObject, x, y, t):
        """
        Overridden method: predict label for x-y point
        """
        # image_data = mediaobj.data()            # cv2 image object containing media data
        # media_path = mediaobj.url               # path to media item
        print(f"CLASSIFYING: {mediaobj.url} | x: {x},  y: {y},  t: {t}")
        classifier_code = random.sample(self.possible_codes, 1)[0]
        prob = round(random.random(), 2)
        # return classifier_code, prob
        return self.create_annotation_label(classifier_code, likelihood=prob)


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(RandoBOT)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)

    args = parser.parse_args()
    bot = RandoBOT(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    bot.start(r)

