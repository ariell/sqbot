import random
from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf, Get
from sqapi.helpers import create_parser


DEFAULT_N_RESAMPLE_THRESH = 20
DEFAULT_N_RETRAIN_THRESH = 20
EXPORT_COLUMNS = [
    # This can be tailored based on the available columns for the /api/annotation/export endpoint, documented here:
    # https://squidle.org/api/help?template=api_help_page.html#annotation
    "id", "tag_names", "comment", "label.id", "label.name", "label.uuid", "annotation_set_id",
    "updated_at", "user.username", "needs_review", "likelihood", "point.is_targeted", "point.x", "point.y",
    "point.media.path_best", "annotation_set_id", "point.polygon",
    # "point.t", "point.media.id", "point.media.key", "point.media.timestamp_start", "annotation_set.name",
    # "point.pose.timestamp", "point.pose.lat", "point.pose.lon", "point.pose.alt",
    # "point.pose.dep", "point.media.deployment.key", "point.media.deployment.campaign.key", "label.color"
]


class ALServiceBot(Annotator):
    def __init__(self, model_path: str = None, n_resample_threshold: int = DEFAULT_N_RESAMPLE_THRESH,
                 n_retrain_threshold: int = DEFAULT_N_RETRAIN_THRESH, **kwargs):
        """
        Active Learning BOT service. Runs as a background service flagging annotations to review in the QA/QC tool.

        :param model_path: the path of the tensorflow model
        :param n_resample_threshold: when the number of flagged for review annotations drops below this, more will be sampled
        :param n_retrain_threshold: the number of new labels required to trigger the retraining of the model
        :param kwargs:
        """
        super().__init__(**kwargs)
        self.model_path = model_path
        self.n_retrain_threshold = n_retrain_threshold
        self.n_resample_threshold = n_resample_threshold
        self.n_used = 0
        self.model = None  # create a real ML model
        self.code2label = dict()
        self.annotation_sets = []

        assert self.poll_delay > 0, \
            "For active learning you need to define a poll_delay in seconds to repeat the AL cycle"

    def export_annotations(self, filters: list):
        """

        :param filters:
        :return:
        """
        q = self.sqapi.export("/api/annotation/export", include_columns=EXPORT_COLUMNS, )\
            .filter(name="annotation_set_id", op="in", val=[s.get("id") for s in self.annotation_sets])
        for f in filters:
            q.filter(**f)
        results = q.execute().json().get("objects")
        print(f"Exported {len(results)} annotations with filter: {filters}...")
        return results

    def retrain_model(self, annotations_labelled):
        """
        Use the labelled annotations to retrain the model
        :param annotations_labelled:
        :return:
        """
        print(f"Retraining model with {len(annotations_labelled)} labelled annotations...")
        self.n_used = len(annotations_labelled)   # store the number of annotations that have been used for training

    def sample_for_review(self, annotations_unlabelled):
        """
        Use an Active Learning sampling method to draw some annotations for review.

        :param annotations_unlabelled:
        :return:
        """
        print(f"Sampling {self.n_resample_threshold} annotations from {len(annotations_unlabelled)} for review...")
        # TODO: this just draws a random sample. You'd need to use your AL sampling metric to make this real.
        annotations_to_review = random.sample(annotations_unlabelled, self.n_resample_threshold)
        for a in annotations_to_review:
            self.sqapi.patch(f"/api/annotation/{a.get('id')}", json_data=dict(needs_review=True)).execute()

    def initialise_points(self, annotation_set, page=1, results_per_page=1000):
        """
        By default, points are populated on images through lazy loading.
        Force points to preload so that all the unlabeled points show up in the export.

        :param annotation_set:
        :param page:
        :param results_per_page:
        :return:
        """
        print("Seeding points in uninitialised images...")
        media_collection_id = annotation_set.get("media_collection",{}).get("id")
        # Get all media objects that occur in this media_collection that do not yet have any points
        r = self.sqapi.get("/api/media", page=page, results_per_page=results_per_page).filter(
            name="media_collections", op="any", val=qf(name="id", op="eq", val=media_collection_id)
        ).filter(
            name=dict(method="has_points", args=[annotation_set.get('id')]), op="eq", val=False
        ).execute().json()

        # Loop through the media objects and make the request to load the points
        for m in r.get("objects"):
            self.sqapi.get(f"/api/media/{m.get('id')}/annotations/{annotation_set.get('id')}").execute().json()

        # If there was more than one page, i.e. lots of media objects, continue processing proceeding pages
        if r.get("total_pages") > page:
            self.initialise_points(annotation_set, page=page+1, results_per_page=results_per_page)

    def run(self, annotation_set_request: Get, **kwargs):
        print("Starting AL cycle...")

        # Get list of annotation_sets
        self.annotation_sets = annotation_set_request.execute().json().get("objects")

        # Force points to preload so that all the unlabeled points show up in the export
        for s in self.annotation_sets:
            self.initialise_points(s)

        # Export labelled annotations (used for training)
        annotations_labelled = self.export_annotations([qf(name="label_id",op="is_not_null")])

        # Export unlabelled annotations which have been flagged for review (to decide whether or not to sample more)
        annotations_unlabelled_flagged = self.export_annotations([qf(name="label_id", op="is_null"), qf(name="needs_review", op="eq", val=True)])

        # Check if the threshold to retrain has been hit
        if (len(annotations_labelled) - self.n_used) > self.n_retrain_threshold:
            self.retrain_model(annotations_labelled)

        # Check if the threshold to resample annotations to review has been hit
        if len(annotations_unlabelled_flagged) < self.n_resample_threshold:
            # Export unlabelled annotations that have not already been sampled for review
            annotations_unlabelled = self.export_annotations([qf(name="label_id", op="is_null"), qf(name="needs_review", op="eq", val=False)])
            # Get more samples for review
            self.sample_for_review(annotations_unlabelled)


if __name__ == '__main__':
    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(ALServiceBot)

    # Add some additional custom cli args not related to the bot class
    parser.add_argument('--annotation_set_id', help="The list of annotation sets to process, separate multiple with spaces", nargs="+", type=int, required=True)

    # Parse the args and initialise the bot
    args = parser.parse_args()
    bot = ALServiceBot(**vars(args))

    # Create a query for the annotation_sets
    r = bot.sqapi.get("/api/annotation_set")
    r.filter(name="id", op="in", val=args.annotation_set_id)

    # Run the bot
    bot.start(r)
