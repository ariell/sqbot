import random
from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf
from sqapi.helpers import cli_init, create_parser
from sqapi.media import SQMediaObject


class RandoBOT(Annotator):
    def __init__(self, **annotator_args):
        """
        RandoBOT automated detector
        An example of an automated object detection bot that selects random class labels to assign to randomly allocated points.
        It provides terrible suggestions, however it provides a simple boiler-plate example of how to integrate a
        Machine Learning algorithm for object detection.
        """
        super().__init__(**annotator_args)
        self.possible_codes = ["ECK", "ASC", "SUB"]

    def detect_points(self, mediaobj: SQMediaObject):
        img = mediaobj.data()  # trigger download to populate width and length
        points = []
        for i in range(5):
            col = int(random.random()*mediaobj.width)
            row = int(random.random()*mediaobj.height)
            classifier_code = random.sample(self.possible_codes, 1)[0]
            prob = round(random.random(), 2)
            points.append(self.create_annotation_label_point_px(classifier_code, prob, row=row, col=col, width=mediaobj.width, height=mediaobj.height))
        return points


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(RandoBOT)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int, required=True)

    args = parser.parse_args()
    bot = RandoBOT(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    bot.start(r)

