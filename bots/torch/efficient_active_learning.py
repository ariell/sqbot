import random
import time

from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf, Get
from sqapi.helpers import create_parser

import os,requests
from PIL import Image
from io import BytesIO
import numpy as np

import torch
import torchvision.transforms as transforms
from torchvision import datasets
from torchvision import models

from .utils_al.dataset_model import FeasDataset, MLP
from .utils_al.utils import get_cosine_schedule_with_warmup, adjust_learning_rate
from .utils_al.sampling_strategy import margin_sampling_model, margin_sampling_pred_model

EXPORT_COLUMNS = [
    # This can be tailored based on the available columns for the /api/annotation/export endpoint, documented here:
    # https://squidle.org/api/help?template=api_help_page.html#annotation
    "id", "tag_names", "comment", "label.id", "label.name", "label.uuid", "annotation_set_id",
    "updated_at", "user.username", "needs_review", "likelihood", "point.is_targeted", "point.x", "point.y",
    "point.media.path_best", "annotation_set_id", "point.polygon",
    # "point.t", "point.media.id", "point.media.key", "point.media.timestamp_start", "annotation_set.name",
    # "point.pose.timestamp", "point.pose.lat", "point.pose.lon", "point.pose.alt",
    # "point.pose.dep", "point.media.deployment.key", "point.media.deployment.campaign.key", "label.color"
]


class EfficientALBot(Annotator):
    def __init__(self, n_resample_threshold: int = 20,
                 n_retrain_threshold: int = 20,
                 batchsize_train: int = 1024,
                 grad_accu: int = 1,
                 batchsize_al_forward: int = 1024 * 2,
                 batchsize_evaluation: int = 1024 * 2,
                 classifier_dim: str = '2048,2048',

                 ###training hyper-parameters
                 train_eps: int = 100,
                 lr: float = 0.1,
                 cls_lr: float = 0.1,
                 momentum: float = 0.9,
                 weight_decay: float = 0,
                 nesterov: bool = True,
                 milestone: str = '60, 80',
                 early_stop: int = 200,

                 ###model path
                 pretrain_path: str = None,
                 model_path: str = None,
                 cropsize: int = 300,
                 crop_media_path: str = None,
                 pre_feas_path: str = None,

                 annotation_set_id: int = None,

                 **kwargs):
        """
        Active Learning BOT service. Runs as a background service flagging annotations to review in the QA/QC tool.

        :param n_resample_threshold: when the number of flagged for review annotations drops below this, more will be sampled
        :param n_retrain_threshold: the number of new labels required to trigger the retraining of the model
        :param poll_delay: active learning poll delay
        :param batchsize_train: batch size during model training
        :param grad_accu: num grad accum for training
        :param batchsize_al_forward: batch size during al forward pass
        :param batchsize_evaluation: batch size during evaluation
        :param classifier_dim: dims of classifier, code would give the dim of the output layer
        :param train_eps: # of training epoch
        :param lr: learning rate
        :param cls_lr: learning rate for classifier
        :param momentum: momentum
        :param weight_decay: weight_decay
        :param nesterov: nesterov
        :param milestone: learning rate schedule (where to drop lr by a ratio)
        :param early_stop: early stop for training
        :param pretrain_path: path of pre-trained model
        :param model_path: path of trained model
        :param cropsize: Crop size for images
        :param crop_media_path: path of download cropped images
        :param pre_feas_path: path of pre-computed features
        :param kwargs:
        """
        super().__init__(**kwargs)
        
        self.code2label = dict()
        self.annotation_sets = []
        
        self.n_retrain_threshold = n_retrain_threshold
        self.n_resample_threshold = n_resample_threshold
        self.n_used = 0
        
        self.batchsize_train=batchsize_train
        self.grad_accu=grad_accu
        self.batchsize_al_forward=batchsize_al_forward
        self.batchsize_evaluation=batchsize_evaluation
        self.classifier_dim=classifier_dim
        self.train_eps=train_eps
        self.lr=lr
        self.cls_lr=cls_lr
        self.momentum=momentum
        self.weight_decay=weight_decay
        self.nesterov=nesterov
        self.milestone=[int(i) for i in milestone.split(',')]
        self.early_stop=early_stop
        self.cropsize=cropsize
        self.pretrain_path=pretrain_path
        # self.model_path=model_path
        self.model_path=model_path or os.path.join(os.path.dirname(pretrain_path), 'res')
        self.crop_media_path=crop_media_path or os.path.join(os.path.dirname(pretrain_path), "data")
        self.pre_feas_path=pre_feas_path or os.path.join(os.path.dirname(pretrain_path), 'totfeas.npy')
        # self.pre_feas_path=pre_feas_path
        self.annotation_set_id = annotation_set_id
        
        self.backbone = models.resnet50()  # backbone
        self.backbone.fc = torch.nn.Identity()
        self.classifier = None # classifier head
        self.random_initial_sampling = True
        
        self.pre_feas = None
        self.need_precompute = True

        self.ids_torch2dict = None
        self.ids_dict2torch = None
        self.ids_dict = None
        self.ids_torch = None
        
        self.num_class = 0 # num classes in labeled set, for set classifier head and label mapping file
        self.label_mapping = None
        self.reverse_mapping = None
        
        self.hybrid_label = None #include all labels, labeled from human and prediction from model
        self.hybrid_prob  = None #probs for model predictions
        
        # self.args = args # all hyper-parameters
    
        assert self.poll_delay > 0, \
            "For active learning you need to define a poll_delay in seconds to repeat the AL cycle"
        assert os.path.isfile(self.pretrain_path), f"Path for pretrain_path: {self.pretrain_path} does not exist"
        # assert os.path.isdir(self.model_path), f"Path for model_path: {self.model_path} does not exist"
        # assert os.path.isdir(self.crop_media_path), f"Path for crop_media_path: {self.crop_media_path} does not exist"
        # assert os.path.isdir(self.pre_feas_path), f"Path for pre_feas_path: {self.pre_feas_path} does not exist"





    def export_annotations(self, filters: list):
        """

        :param filters:
        :return:
        """
        q = self.sqapi.export(f"/api/annotation_set/{self.annotation_set_id}/export", include_columns=EXPORT_COLUMNS)
        for f in filters:
            q.filter(**f)
        results = q.execute().json().get("objects")
        print(f"Exported {len(results)} annotations with filter: {q._filters}...")
        return results

    def order_file(self, ids_dict, ids_torch):
        
        torch2dict = []
        for i in ids_torch:
            torch2dict += [ids_dict.index(i)]
        
        dict2torch = []
        for i in ids_dict:
            dict2torch += [ids_torch.index(i)]
        
        self.ids_torch2dict = torch2dict
        self.ids_dict2torch = dict2torch
        
        return

    def download_crop_img(self, cropsize): # try to download all images in annotation_set_id, crop around the point annotations
        
        allset = self.export_annotations([]) # get all data in annotation_set_id
        
        self.hybrid_label = -1*np.ones(len(allset))  #initialize hybrid_label as -1
        self.hybrid_prob  = 0*np.ones(len(allset)) #initialize probs as 0
        
        if not os.path.exists(self.crop_media_path):
            os.makedirs(self.crop_media_path)
        #download and crop images 
        ids_dict = []
        reload_medianame = None
        for isamp in allset:
            medianame = os.path.basename(isamp['point']['media']['path_best'])
            if len(medianame) == 0:
                medianame = os.path.dirname(isamp['point']['media']['path_best'])
                medianame =  os.path.basename(medianame)
            
            if medianame != reload_medianame:# do not re-request image for different point within the same image
                reload_medianame = medianame
            
                img = requests.get(isamp['point']['media']['path_best']).content
                img = BytesIO(img)
                img = Image.open(img)
        
            if 'x' in isamp['point']:# sometimes the "point" does not include this key 
                width, height = img.size#
                x,y = isamp['point']['x'], isamp['point']['y']
                x,y = x*width, y*height
                
                box = ( max(0, int(x - cropsize / 2)), max( int(y - cropsize / 2), 0),  min(width, int(x + cropsize / 2)), min( int(y + cropsize / 2), height)  )
                cropimg = img.crop(box)
                
                cropimg.save(os.path.join(self.crop_media_path, str(isamp['id']) + '.jpg' ))
                ids_dict += [str(isamp['id'])]
        
        ### there is different sample order between torch dataset and allset dict
        ids_torch = ids_dict.copy()
        ids_torch.sort()
        
        self.ids_dict = ids_dict
        self.ids_torch = ids_torch
        
        self.order_file(ids_dict, ids_torch)    
            
        return
    
    def to_Torch_dataset(self): #build torch datdaset
        
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
        transform = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean, std)])
        
        allset_torch = datasets.ImageFolder(root= os.path.dirname(self.crop_media_path), transform=transform)
        # allset_torch = datasets.ImageFolder(root=self.crop_media_path, transform=transform)

        return allset_torch
    
    def load_pretrain_backbone(self): # load pre-trained model for computing features 
        
        checkpoint = torch.load(self.pretrain_path, map_location=torch.device('cpu'))
        # checkpoint = torch.load(self.pretrain_path, map_location=lambda storage, loc: storage)

        encoder_dict = self.backbone.state_dict()
        state_dict = {k[27:]:v for k,v in checkpoint['state_dict'].items() if k[27:] in encoder_dict.keys()}#byol eman 
        encoder_dict.update(state_dict)
        msg = self.backbone.load_state_dict(encoder_dict)
        print('load pretrained model ', len(state_dict), msg)
        
        return
    
    def pre_compute_feas(self, allset_torch):
        
        allset_loader = torch.utils.data.DataLoader(
            dataset = allset_torch, 
            batch_size=512,
            shuffle=False,###extract feas ---> False, linear eva ---> True
            drop_last=False,
            # num_workers = 16,
            #pin_memory=True
        )
        
        #load pre-trained weights for backbone
        self.load_pretrain_backbone()
        
        #
        self.backbone.eval()
        self.backbone.cuda()
        with torch.no_grad():
            totfeas = []
            for idx, (images, _) in enumerate(allset_loader):
                feature = self.backbone(images.cuda())  
                print(idx)
                if len(totfeas) == 0:
                    totfeas = feature.cpu().numpy().copy() 
                else:
                    totfeas = np.vstack((totfeas, feature.cpu().numpy().copy())) 
        
        ### align totfeas order to annotation_set dict 
        totfeas = totfeas[self.ids_torch2dict,:]
        self.pre_feas = totfeas
        np.save(self.pre_feas_path, totfeas)
        
        return
    
    def construct_label_mapping(self, annotations_labelled):
    
        label_mapping = {}
        num = 0
        for i in annotations_labelled:
            if i['label']['id'] not in label_mapping:
                label_mapping[i['label']['id']] = {'torch_label':num, 'name':i['label']['name']}
                num += 1
        
        self.num_class = len(label_mapping)
        self.label_mapping = label_mapping
        
        #construct reversed mapping from torch label 2 sq label id
        reverse_mapping = {}
        for k,v in label_mapping.items():
            reverse_mapping[v['torch_label']] = {'labelid':k, 'name':v['name']}
        self.reverse_mapping = reverse_mapping
        
        return

    def retrain_model(self, annotations_labelled):# re-train the proxy model (classifier head) for fast sample selection
        """
        Use the labelled annotations to retrain the model
        :param annotations_labelled:
        :return:
        """
        print(f"Retraining model with {len(annotations_labelled)} labelled annotations...")
        self.n_used = len(annotations_labelled)   # store the number of annotations that have been used for training
        
        #construct torch dataset
        lbl_idx = [ self.ids_dict.index(str(i['id'])) for i in annotations_labelled ]
        
        self.construct_label_mapping(annotations_labelled)
        lbl_label = np.array([ self.label_mapping[i['label']['id']]['torch_label'] for i in annotations_labelled ])
        
        #update human labels into self.hybrid_label
        labelid_from_human = np.array([ i['label']['id'] for i in annotations_labelled ])
        self.hybrid_label[lbl_idx] = labelid_from_human
        self.hybrid_prob[lbl_idx] = 1 # update probs of human labels as 1
        
        trainset = FeasDataset(self.pre_feas[lbl_idx,:], lbl_label, None)
        
        if len(trainset) < self.batchsize_train:
            Droplast = False
        else:
            Droplast = True
        train_loader = torch.utils.data.DataLoader(
            trainset,
            batch_size = self.batchsize_train,
            shuffle=True,
            drop_last = Droplast
        )
        
        #initialize classifier 
        indim_classifier, hiddim_classifier = self.classifier_dim.split(',')
        
        self.classifier = MLP( int(indim_classifier), [int(hiddim_classifier)], self.num_class)
        self.classifier = self.classifier.train().cuda()
        
        #training
        self.classifier, trainloss = self.train_mlp(train_loader, self.classifier)
        #save trained model, just classifier, the pre-trained model is always used as backbone
        if not os.path.exists(self.model_path):
            os.makedirs(self.model_path)
        torch.save({'backbone_state_dict': None, 'classifier_state_dict': self.classifier.state_dict()}, os.path.join(self.model_path, 'checkpoint.pth.tar'))

    def train_mlp(self, train_loader, classifier):

        optimizer = torch.optim.SGD(
            classifier.parameters(),
            lr=self.lr,
            momentum=self.momentum,
            weight_decay=self.weight_decay,  # 0.0003,
            nesterov=self.nesterov
        )

        num_epoch = self.train_eps

        scheduler = get_cosine_schedule_with_warmup(optimizer, 0, num_epoch * len(train_loader))

        classifier.train()

        CE = torch.nn.CrossEntropyLoss(reduction='mean')

        trainloss = []

        for ep in range(num_epoch):
            for cur_iter, (inputs, labels, _) in enumerate(train_loader):
                inputs = inputs.type(torch.cuda.FloatTensor)
                inputs, labels = inputs.cuda(), labels.cuda(non_blocking=True)

                preds = classifier(inputs)
                # Compute the loss
                loss = CE(preds, labels.long())  # (preds, labels)
                # Perform the backward pass

                trainloss += [loss.item()]
                optimizer.zero_grad()
                loss.backward()
                # Update the parametersSWA
                optimizer.step()
                scheduler.step()

            print(ep, loss.item(), (preds.argmax(axis=1) == labels).sum())

        # totpre, totemb = get_output_emb(all_loader, model, classifier)

        return classifier, trainloss

    def train_fine_tune(self, train_loader, model, classifier):

        s = time.time()

        if classifier is not None:
            optimizer = torch.optim.SGD([
                {'params': model.parameters()},
                {'params': classifier.parameters(), 'lr': self.cls_lr}  ###0.1
            ], self.lr, momentum=self.momentum, weight_decay=self.weight_decay, nesterov=self.nesterov)  ###0.001
        else:
            feature_params, classifier_params = [], []
            feature_names, classifier_names = [], []
            for name, param in model.named_parameters():
                if 'fc.' in name:
                    classifier_params += [param]
                    classifier_names += [name]
                else:
                    feature_params += [param]
                    feature_names += [name]

            optimizer = torch.optim.SGD([
                {'params': feature_params},
                {'params': classifier_params, 'lr': self.cls_lr}
            ], self.lr, momentum=self.momentum, weight_decay=self.weight_decay, nesterov=self.nesterov)  #

        num_epoch = self.train_eps

        # scheduler = get_cosine_schedule_with_warmup( optimizer, 0, num_epoch*len(train_loader) )

        model.train()
        if classifier is not None:
            classifier.train()

        trainloss = []

        CE = torch.nn.CrossEntropyLoss(reduction='mean')
        optimizer.zero_grad()
        for epoch in range(num_epoch):

            adjust_learning_rate(optimizer, epoch, self.train_eps, self.lr, self.cls_lr, False, self.milestone)
            if epoch % 10 == 1:
                print('time', time.time() - s)

            for idx, (images, labels) in enumerate(train_loader):

                preds = model(images.cuda())  # classifier(feature)#
                if classifier is not None:
                    preds = classifier(preds)

                loss = CE(preds, labels.long().cuda())
                trainloss += [loss.item()]
                loss = loss / self.grad_accu
                loss.backward()
                if idx % self.grad_accu == 0:
                    optimizer.step()
                    optimizer.zero_grad()
                # lr = lr_scheduler.step()
                # scheduler.step()

            if epoch >= (self.early_stop - 1):
                break

        return model, classifier, trainloss

    def sample_for_review(self, annotations_unlabelled):
        """
        Use an Active Learning sampling method to draw some annotations for review.

        :param annotations_unlabelled:
        :return:
        """
        print(f"Sampling {self.n_resample_threshold} annotations from {len(annotations_unlabelled)} for review...")
                
        #active learning sampling
        if self.random_initial_sampling or self.classifier is None:  # some active learning strategies needs random initial set
            self.random_initial_sampling = False    
            annotations_to_review = random.sample(annotations_unlabelled, self.n_resample_threshold)
            
        else:
            #construct ulbl torch dataset
            ulbl_idx = [ self.ids_dict.index(str(i['id'])) for i in annotations_unlabelled ]
            ulbl_ids = [ i['id'] for i in annotations_unlabelled ]
            
            unlblset = FeasDataset(self.pre_feas[ulbl_idx,:], np.array([0]*len(ulbl_idx)), None)
            unlbl_loader = torch.utils.data.DataLoader(
                unlblset,
                batch_size = self.batchsize_al_forward,
                shuffle = False,
                drop_last = False
            )
            
            newidx, preds_unlbl, probs_unlbl = margin_sampling_pred_model(unlbl_loader, self.classifier, None, self.n_resample_threshold, args)
            annotations_to_review = [annotations_unlabelled[i] for i in newidx]#
            preds_unlbl = np.array([ self.reverse_mapping[i]['labelid'] for i in preds_unlbl ])
            
            #update self.hybrid_label with model prediction
            change_idx = np.argwhere( preds_unlbl != self.hybrid_label[ulbl_idx] )[:,0]  # change_idx is the order in annotations_unlabelled dataset
            change_idx1 = np.array(ulbl_idx)[change_idx]  # change_idx1 is the order in all dataset
            self.hybrid_label[ulbl_idx] = preds_unlbl
            self.hybrid_prob[ulbl_idx] = probs_unlbl

            # for labels that have changed,
            for i in change_idx:
                if probs_unlbl[i] > 0.99:
                    data = dict(label_id=int(preds_unlbl[i]), likelihood=float(probs_unlbl[i]), needs_review=True)
                    self.sqapi.patch(f"/api/annotation/{ulbl_ids[i]}", json_data=data).execute()

        for a in annotations_to_review:
            self.sqapi.patch(f"/api/annotation/{a.get('id')}", json_data=dict(needs_review=True)).execute()

    def initialise_points(self, annotation_set, page=1, results_per_page=1000):
        """
        By default, points are populated on images through lazy loading.
        Force points to preload so that all the unlabeled points show up in the export.

        :param annotation_set:
        :param page:
        :param results_per_page:
        :return:
        """
        print("Seeding points in uninitialised images...")
        media_collection_id = annotation_set.get("media_collection",{}).get("id")
        # Get all media objects that occur in this media_collection that do not yet have any points
        r = self.sqapi.get("/api/media", page=page, results_per_page=results_per_page).filter(
            name="media_collections", op="any", val=qf(name="id", op="eq", val=media_collection_id)
        ).filter(
            name=dict(method="has_points", args=[annotation_set.get('id')]), op="eq", val=False
        ).execute().json()

        # Loop through the media objects and make the request to load the points
        for m in r.get("objects"):
            self.sqapi.get(f"/api/media/{m.get('id')}/annotations/{annotation_set.get('id')}").execute().json()

        # If there was more than one page, i.e. lots of media objects, continue processing proceeding pages
        if r.get("total_pages") > page:
            self.initialise_points(annotation_set, page=page+1, results_per_page=results_per_page)

    def run(self, annotation_set_request: Get, **kwargs):
        print("Starting AL cycle...")

        # Get list of annotation_sets
        self.annotation_sets = annotation_set_request.execute().json().get("objects")

        # Force points to preload so that all the unlabeled points show up in the export
        for s in self.annotation_sets:
            self.initialise_points(s)

        ###########
        #preparing for efficient AL
        if self.need_precompute:
            self.download_crop_img(self.cropsize)
            allset_torch = self.to_Torch_dataset()
            self.pre_compute_feas(allset_torch)
            self.need_precompute = False


        # Export labelled annotations (used for training)
        annotations_labelled = self.export_annotations([qf(name="label_id",op="is_not_null")])
        if len(annotations_labelled) >= self.n_retrain_threshold:
            self.random_initial_sampling = False

        # Export unlabelled annotations which have been flagged for review (to decide whether or not to sample more)
        annotations_unlabelled_flagged = self.export_annotations([qf(name="label_id", op="is_null"), qf(name="needs_review", op="eq", val=True)])
        

        # Check if the threshold to retrain has been hit
        if (len(annotations_labelled) - self.n_used) >= self.n_retrain_threshold:
            self.retrain_model(annotations_labelled)

        # Check if the threshold to resample annotations to review has been hit
        if len(annotations_unlabelled_flagged) < self.n_resample_threshold:
            # Export unlabelled annotations that have not already been sampled for review
            annotations_unlabelled = self.export_annotations([qf(name="label_id", op="is_null")])
            # Get more samples for review
            self.sample_for_review(annotations_unlabelled)


if __name__ == '__main__':
    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(EfficientALBot)
    
    # Add some additional custom cli args not related to the bot class
    # parser.add_argument('--annotation_set_id', help="The list of annotation sets to process, separate multiple with spaces", nargs="+", type=int, required=True)
    parser.add_argument('--annotation_set_id', help="The list of annotation sets to process, separate multiple with spaces", type=int, required=True)

    # Parse the args and initialise the bot
    args = parser.parse_args()
    bot = EfficientALBot(**vars(args))
    
    # Create a query for the annotation_sets
    r = bot.sqapi.get("/api/annotation_set")
    r.filter(name="id", op="eq", val=args.annotation_set_id)
    
    # Run the bot
    bot.start(r)
