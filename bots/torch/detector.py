from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf
from sqapi.helpers import cli_init, create_parser
from sqapi.media import SQMediaObject
import cv2

import torch
from omegaconf import DictConfig
from PIL import Image

from .utils.get_faster_rcnn_model import get_faster_rcnn_model as get_model
from .utils.utils_dataset import get_transform


class TorchBOTDetector(Annotator):
    def __init__(self, model_path: str, device: str = "cpu", **kwargs: object) -> object:
        """
        Detector using Torch.
        This detector generated bounding boxes that are submitted to SQ+

        :param model_path: the path of the torch model checkpoint
        :param device:
        :param kwargs:
        """
        super().__init__(**kwargs)
        self.checkpoint = torch.load(model_path, map_location="cpu")
        self.opt = DictConfig({'train': self.checkpoint['self.opt.train']})
        self.model = get_model(self.opt)
        self.device = device
        self.model.to(self.device)
        self.model.load_state_dict(self.checkpoint['model'])

    def tensor2polygon(self, box):
        # Convert prediction boxes to pixel coordinates
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])

        return [[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]]

    def detect_points(self, mediaobj: SQMediaObject):
        if not mediaobj.is_processed:
            orig_image = mediaobj.data()
            img = mediaobj.data(Image.fromarray(cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)))  # convert to PIL Image
        else:
            img = mediaobj.data()  # will return processed image

        transform = get_transform(False, self.opt.train)
        img, target = transform(img, None)
        images = list(img.to(self.device) for img in [img])

        # Use the models and visualize the prediction
        self.model.eval()
        if torch.cuda.is_available():
            torch.cuda.synchronize()
        outputs = self.model(images)
        outputs = [{k: v.to("cpu") for k, v in t.items()} for t in outputs]
        prediction = outputs[0]

        # Draw prediction on the image and save image
        points = []
        boxes = prediction['boxes'].detach().cpu()
        for i in range(len(prediction['labels'])):
            likelihood = prediction['scores'].detach().cpu()[i]
            code = int(prediction['labels'][i])
            if likelihood > self.prob_thresh:
                polygon = self.tensor2polygon(boxes[i, :])
                p = self.create_annotation_label_point_px(
                    code, likelihood=likelihood, polygon=polygon, width=mediaobj.width, height=mediaobj.height
                )
                points.append(p)

        return points


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(TorchBOTDetector)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)

    args = parser.parse_args()
    bot = TorchBOTDetector(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    bot.start(r)

