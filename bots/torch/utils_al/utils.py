# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 16:41:20 2023

@author: wenzt
"""

import numpy as np
import math
import time
    
import torch
import torch.nn.functional as F
from torch.optim.lr_scheduler import LambdaLR



def get_cosine_schedule_with_warmup(optimizer,
                                    num_warmup_steps,
                                    num_training_steps,
                                    num_cycles=7./16.,
                                    last_epoch=-1):
    def _lr_lambda(current_step):
        if current_step < num_warmup_steps:
            return float(current_step) / float(max(1, num_warmup_steps))
        no_progress = float(current_step - num_warmup_steps) / \
            float(max(1, num_training_steps - num_warmup_steps))
        return max(0., math.cos(math.pi * num_cycles * no_progress))

    return LambdaLR(optimizer, _lr_lambda, last_epoch)

def adjust_learning_rate(optimizer, epoch, totepo, lr, lr_classifier, lrcos, schedule_milstone):
    """Decay the learning rate based on schedule"""
    num_groups = len(optimizer.param_groups)
    assert 1 <= num_groups <= 2
    lrs = []
    if num_groups == 1:
        lrs += [lr]
    elif num_groups == 2:
        lrs += [lr, lr_classifier]
    assert len(lrs) == num_groups
    for group_id, param_group in enumerate(optimizer.param_groups):
        lr = lrs[group_id]
        if lrcos:  # cosine lr schedule
            lr *= 0.5 * (1. + math.cos(math.pi * epoch / totepo))
        else:  # stepwise lr schedule
            for milestone in schedule_milstone:
                lr *= 0.1 if epoch >= milestone else 1.
        param_group['lr'] = lr


def evaluation(test_loader, classifier, model = None):

    testpre_m, testl = [],[]
    if classifier is None and model is None:
        print('wrong evaluation model setting')
        return None, None
    
    if model is not None:
        model.eval()
    if classifier is not None:
        classifier.eval()
        
    for cur_iter, (inputs, labels,_) in enumerate(test_loader):
        if cur_iter % 10 == 1:
            print(cur_iter) 
        inputs = inputs.type(torch.cuda.FloatTensor)
        inputs, labels = inputs.cuda(), labels.cuda(non_blocking=True)
        with torch.no_grad():
            if model is not None:
                if classifier is None:
                    preds = model(inputs)
                else:
                    preds = classifier(model(inputs))
            else:    
                preds = classifier(inputs)
                
        if len(testpre_m) == 0:
            testpre_m = preds.cpu().numpy()
        else:
            testpre_m = np.vstack((testpre_m, preds.cpu().numpy() ))
        testl += labels.cpu().numpy().tolist()
    
    tspre = testpre_m.argmax(axis=1)    
    tacc = (tspre == np.array(testl)).sum() / len(testl)
    # acc += [tacc]
    print('test acc: ', tacc)  
    
    if classifier is not None:
        classifier.train()
    if model is not None:
        model.train()
    
    return tacc