# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 14:58:59 2023

@author: wenzt
"""

import numpy as np
from torch.utils.data import Dataset
import torch

class FeasDataset(Dataset):
    def __init__(self, x, y, noise_scale):
        self.x = x
        self.y = y
        self.noise_scale = noise_scale

    def __len__(self):
        return len(self.y) 

    def __getitem__(self, idx):
        if self.noise_scale is None:
            return self.x[idx,:], self.y[idx], idx
        else:
            return np.random.normal(self.x[idx,:], scale = self.noise_scale[self.y[idx],:]), self.y[idx], idx
        
        
        
    
class MLP(torch.nn.Module):
    def __init__(self, in_dim, hidden_dim=[2048], out_dim=2048):
        super().__init__()
        self.layer1 = torch.nn.Sequential(
            torch.nn.Linear(in_dim, hidden_dim[0]),
            torch.nn.BatchNorm1d(hidden_dim[0]),
            torch.nn.ReLU(inplace=True)
        )
        
        if len(hidden_dim) == 1:
            self.layer2 = torch.nn.Sequential(
                torch.nn.Linear(hidden_dim[0], out_dim)
                # torch.nn.BatchNorm1d(out_dim)
            )
            self.num_layers = 1
        else:
            self.layer2 = torch.nn.Sequential(
                torch.nn.Linear(hidden_dim[0], hidden_dim[1]),
                torch.nn.BatchNorm1d(hidden_dim[1]),
                torch.nn.ReLU(inplace=True)
            )
            self.layer3 = torch.nn.Sequential(
                torch.nn.Linear(hidden_dim[1], out_dim)
                # torch.nn.BatchNorm1d(out_dim)
            )
            self.num_layers = 2
            
        self.emb = None
        
    # def set_layers(self, num_layers):
    #     self.num_layers = num_layers

    def forward(self, x):
        
        x = self.layer1(x)
        if self.num_layers == 1:
            self.emb = x.clone()
            x = self.layer2(x)
        else:
            x = self.layer2(x)
            self.emb = x.clone()
            x = self.layer3(x)
        return x     
    