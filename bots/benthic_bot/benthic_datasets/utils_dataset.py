import torch.utils
import torchvision

from .presets import DetectionPresetTrain, DetectionPresetEval
from .utils_coco import num_annotations, get_dataset


def get_dataset_desc(dataset, split):
    # todo: Return a list if dataset has more than one folder
    return {"folder": dataset.folder,
            "type": dataset.type,
            "split": get_split(split, dataset)}


def get_split(default_split, config):
    # Use the split in the config if there is one other use default_split
    config_split = config.get("split", None)
    return default_split if config_split is None else config_split

def get_transform(train, opt_train, data_augmentation=None, shortest_side_dict={}):
    data_augmentation = opt_train.data_augmentation if data_augmentation is None else data_augmentation
    if train:
        return DetectionPresetTrain(data_augmentation=data_augmentation, shortest_side_dict=shortest_side_dict, synthetic_paths=opt_train.synthetic_paths)
    elif opt_train.weights and opt_train.test_only:
        weights = torchvision.models.get_weight(opt_train.weights)
        trans = weights.transforms()
        return lambda img, target: (trans(img), target)
    else:
        return DetectionPresetEval()

def get_datasets(datasets, transform, data_path, is_source=True):
    if isinstance(datasets, dict):
        return get_dataset(datasets, transform, data_path, is_source=is_source)
    elif datasets[0]["split"] == "test":
        return get_dataset(datasets[0], transform, data_path, is_source=is_source)
    else:
        # todo: Only works for two datasets
        datasets = []
        for i in range(len(datasets)):
            datasets.append(get_dataset(datasets[i], transform, data_path, is_source=is_source))
        return FusedDataset(datasets[0], datasets[1])


def print_and_log_dataloader_info(dataloaders, logger=None):
    message = "Dataloader info: "
    for n in ['train', 'target', 'test', 'psuedo']:
        ds = dataloaders.get(n, None)
        if ds is not None:
            message = f"{message} {n} images: {len(ds)}, {n} annotations: {num_annotations(ds.dataset)}; "
            if logger is not None:
                logger[f"dataset/{n}"].append(len(ds))
                logger[f"dataset/{n}_ann"].append(num_annotations(ds.dataset))
    print(message[:-2])


class FusedDataset(torch.utils.data.Dataset):
    def __init__(self, dataset1, dataset2):
        self.dataset1 = dataset1
        self.dataset2 = dataset2
        super().__init__()

    def num_annotations(self):
        return num_annotations(self.dataset1) + num_annotations(self.dataset2)

    def __getitem__(self, idx):
        if idx < len(self.dataset1):
            return self.dataset1.__getitem__(idx)
        else:
            return self.dataset2.__getitem__(idx - len(self.dataset1))

    def __len__(self):
        return len(self.dataset1) + len(self.dataset2)
