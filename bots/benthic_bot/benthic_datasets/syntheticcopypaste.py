import random
from typing import Dict, Tuple

import cv2
import numpy as np
import torch
from PIL import Image
from torch import Tensor
from torchvision.transforms import functional as F, transforms as T

from .compose import Compose
from .transforms import RandomHorizontalFlip, RandomVerticalFlip, resize_boxes
from .utils_coco import get_dataset


def _synthetic_copy_paste(
        image,
        target: Dict[str, Tensor],
        paste_image,
        paste_target: Dict[str, Tensor],
        other_mask,
        blending: bool = True,
        resize_interpolation: F.InterpolationMode = F.InterpolationMode.BILINEAR,
) -> Tuple[torch.Tensor, Dict[str, Tensor]]:
    # Random paste targets selection:
    transform = T.Compose([T.PILToTensor()])
    num_masks = len(paste_target["masks"]) if 'masks' in paste_target else 0
    size_target = image.size

    if num_masks == 0 and other_mask is None:
        # Such degerante case with num_masks=0 can happen with LSJ
        # Let's just return (image, target)
        return image, target
    else:
        if num_masks > 0:
            # We have to please torch script by explicitly specifying dtype as torch.long
            random_selection = torch.randint(0, num_masks, (num_masks,))
            random_selection = torch.unique(random_selection).to(torch.long)

            paste_masks = paste_target["masks"][random_selection]
            paste_boxes = paste_target["boxes"][random_selection]
            paste_labels = paste_target["labels"][random_selection]
            if other_mask is not None:  # and other_mask.shape[0] > 1:
                other_mask = transform(other_mask)
                # other_mask = other_mask.unsqueeze(0)
                paste_masks += other_mask.to(torch.bool)

        image = transform(image)
        paste_image = transform(paste_image)
        size1 = image.shape[-2:]
        size2 = paste_image.shape[-2:]

        # We resize source and paste data if they have different sizes
        # This is something we introduced here as originally the algorithm works
        # on equal-sized data (for example, coming from LSJ data augmentations)
        resize_interpolation = F.InterpolationMode.BILINEAR
        if size1 != size2:
            paste_image = F.resize(paste_image, size1, interpolation=resize_interpolation)
            if num_masks > 0:
                paste_masks = F.resize(paste_masks, size1, interpolation=resize_interpolation)
                # resize bboxes:
                ratios = torch.tensor((size1[1] / size2[1], size1[0] / size2[0]), device=paste_boxes.device)
                paste_boxes = paste_boxes.view(-1, 2, 2).mul(ratios).view(paste_boxes.shape)
            if other_mask is not None:
                other_mask = F.resize(other_mask, size1, interpolation=resize_interpolation)

        if num_masks > 0:
            paste_alpha_mask = paste_masks.sum(dim=0) > 0
        else:
            other_mask = transform(other_mask)
            paste_alpha_mask = other_mask > 0

        # Use target masks to make space for bbox underneath
        if "masks" in target:
            target_masks = target["masks"].sum(dim=0) > 0
            #print(f"alpha {paste_alpha_mask.shape}, target_mask {target_masks.shape}, image {image.shape}")
            paste_alpha_mask = (paste_alpha_mask.to(int) - target_masks.to(int)) > 0

    if blending:
        paste_alpha_mask = F.gaussian_blur(
            paste_alpha_mask.unsqueeze(0),
            kernel_size=(5, 5),
            sigma=[
                2.0,
            ],
        )

    paste_alpha_mask = paste_alpha_mask.squeeze()
    image = (image * (~paste_alpha_mask)) + (paste_image * paste_alpha_mask)
    image = F.to_pil_image(image)
    # image.save("maskedv2.jpg")

    # Copy-paste masks:
    # masks = masks * (~paste_alpha_mask)
    # non_all_zero_masks = masks.sum((-1, -2)) > 0
    # masks = masks[non_all_zero_masks]

    # Do a shallow copy of the target dict
    out_target = {k: v for k, v in target.items()}

    # out_target["masks"] = torch.cat([masks, paste_masks])

    # Copy-paste boxes and labels
    # boxes = ops.masks_to_boxes(masks)
    if num_masks > 0:
        if "boxes" in out_target and len(out_target['boxes']) > 0:
            out_target["boxes"] = torch.cat(out_target["boxes", paste_boxes])
            # labels = target["labels"][non_all_zero_masks]
            out_target["labels"] = torch.cat(out_target["labels", paste_labels])
        else:
            out_target["boxes"] = paste_boxes
            out_target["labels"] = paste_labels

        # Check for degenerated boxes and remove them
        boxes = out_target["boxes"]
        degenerate_boxes = boxes[:, 2:] <= boxes[:, :2]
        if degenerate_boxes.any():
            valid_targets = ~degenerate_boxes.any(dim=1)

            out_target["boxes"] = boxes[valid_targets]
            # out_target["masks"] = out_target["masks"][valid_targets]
            out_target["labels"] = out_target["labels"][valid_targets]

            if "area" in out_target:
                out_target["area"] = out_target["area"][valid_targets]
            if "iscrowd" in out_target and len(out_target["iscrowd"]) == len(valid_targets):
                out_target["iscrowd"] = out_target["iscrowd"][valid_targets]

    return image, out_target


class SyntheticCopyPaste(torch.nn.Module):
    def __init__(self, blending=True, resize_interpolation=F.InterpolationMode.BILINEAR, p=0.3,
                 synthetic_path=["./"],
                 copypaste_dataset={"folder": "squidle_handfish_pretrain", "split": "train", "type": "coco"},
                 data_path="./outputs",
                 categories_of_interest=[],
                 random_shift=False,
                 crop_range=[0.3, 1.0],
                 bbox_cp=False,
                 paste_image_kept_p=0.5):
        super().__init__()
        # Dirty hack here so that we only load dataset for paste images once.
        if 'syntheticcopypaste_ds' not in globals():
            global syntheticcopypaste_ds
            syntheticcopypaste_ds = get_dataset(copypaste_dataset, None, data_path,
                                                is_source=True)
        self.resize_interpolation = resize_interpolation
        self.blending = blending
        self.p = p
        self.categories_of_interest = categories_of_interest
        self.synthetic_path = synthetic_path
        self.data_path = data_path
        self.copypaste_dataset = copypaste_dataset
        self.random_shift = random_shift
        self.crop_range = crop_range
        self.paste_image_kept_p = paste_image_kept_p
        self.bbox_cp = bbox_cp

    def forward(self, image, target: Dict[str, Tensor]):
        # images = [t1, t2, ..., tN]
        # Let's define paste_images as shifted list of input images
        # paste_images = [t2, t3, ..., tN, t1]
        # FYI: in TF they mix data on the dataset level

        if torch.rand(1) >= self.p:
            return image, target

        paste_image, paste_target, other_mask = self.get_paste_data_fast()

        # Apply random horizontal or vertical flip to paste_image
        p_transform = 0.5
        h_transform = Compose([RandomHorizontalFlip(p=1.0)])
        v_transform = Compose([RandomVerticalFlip(p=1.0)])
        if torch.rand(1) < p_transform:
            paste_image, paste_target = h_transform(paste_image, paste_target)
            if other_mask is not None:
                other_mask, _ = h_transform(other_mask, None)
        if torch.rand(1) < p_transform:
            paste_image, paste_target = v_transform(paste_image, paste_target)
            if other_mask is not None:
                other_mask, _ = v_transform(other_mask, None)

        self.add_masks(image, target, self.bbox_cp)
        if self.random_shift:
            if torch.rand(1) < 0.5:
                image, target, _ = random_crop(image, target, crop_range=self.crop_range)
            else:
                paste_image, paste_target, other_mask = random_crop(paste_image, paste_target,
                                                                    other_mask,
                                                                    crop_range=self.crop_range)

        if image.size > paste_image.size:
            image, target = self.resize_image_and_target(image, target, paste_image.size[-1])
        elif image.size < paste_image.size:
            paste_image, paste_target = self.resize_image_and_target(paste_image, paste_target, image.size[-1])
            if other_mask is not None:
                other_mask, _ = self.resize_image_and_target(other_mask, None, image.size[-1])

        # copypaste paste image masks to image.
        paste_image_kept = torch.rand(1) < self.paste_image_kept_p
        # print(paste_image_kept, paste_image.size, image.size, paste_image.size >= image.size)

        # Check that novel class image is smaller or the same size as base class image
        if paste_image_kept:
            output_image, output_data = _synthetic_copy_paste(
                paste_image,
                paste_target,
                image,
                target,
                None,
                blending=self.blending,
                resize_interpolation=self.resize_interpolation
            )
        else:
            output_image, output_data = _synthetic_copy_paste(
                image,
                target,
                paste_image,
                paste_target,
                other_mask,
                blending=self.blending,
                resize_interpolation=self.resize_interpolation,
            )

        # from torchvision.transforms.functional import to_pil_image
        # to_pil_image(output_image).save("masked_image.jpg")
        return output_image, output_data

    def resize_image_and_target(self, image, target, new_height):
        original_size = image.size
        hpercent = (new_height / float(image.size[-1]))
        wsize = int((float(image.size[0]) * float(hpercent)))
        image = image.resize((wsize, new_height), Image.Resampling.LANCZOS)
        if target is not None:
            if "masks" in target:
                mask = target["masks"]
                mask = torch.nn.functional.interpolate(
                    mask[:, None].float(), size=(image.size[1], image.size[0])
                )[:, 0].byte()
                target["masks"] = mask
            if len(target["boxes"]) > 0:
                target["boxes"] = resize_boxes(target["boxes"], original_size=original_size, new_size=image.size)
        return image, target

    def add_masks(self, image, target, bbox_cp=False):
        # Add masks to target dict
        if 'polygons' in target and not bbox_cp:
            # polygon = [(x1,y1),(x2,y2),...] or [x1,y1,x2,y2,...]
            # width = ?
            # height = ?
            num_polygons = len(target['polygons'])
            masks = torch.zeros((num_polygons, image.size[1], image.size[0]))
            for i in range(num_polygons):
                polygon = target['polygons'][i]
                img = np.zeros((image.size[1], image.size[0]), np.uint8)
                if len(polygon) > 0:
                    cv2.drawContours(img, [np.array(polygon)], -1, 1, -1)
                # else:
                #    print(f"No contours: {polygon}, {img.shape}")
                masks[i] = torch.tensor(img, dtype=torch.int64)
            target['masks'] = masks
        else:
            num_boxes = target['boxes'].shape[0]
            masks = torch.zeros((num_boxes, image.size[1], image.size[0]))
            for i in range(num_boxes):
                bbox = target['boxes'][i]
                masks[i, int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])] = 1
            target['masks'] = masks

    def __repr__(self) -> str:
        s = f"{self.__class__.__name__}(blending={self.blending}, resize_interpolation={self.resize_interpolation})"
        return s

    def get_paste_data_fast(self):
        # Gets a randomly selected paste image and annotations.
        # If label is a category of interest keeps annotations, otherwise uses mask into other_mask
        # to be used for negative examples.
        # Be careful to use with labels that don't have the same number ie no overlap.

        ds = syntheticcopypaste_ds
        i = random.randint(0, len(ds) - 1)
        paste_image, paste_target = ds[i]
        self.add_masks(paste_image, paste_target)

        # Iterate through labels and split between target and other.
        idx_to_keep = []
        idx_for_other = []
        for idx, label in enumerate(paste_target["labels"]):
            if label in self.categories_of_interest:
                idx_to_keep.append(idx)
            else:
                idx_for_other.append(idx)

        if len(idx_for_other) > 0:
            other_masks = paste_target["masks"][idx_for_other]
            other_mask = torch.sum(other_masks, dim=0)
            other_mask = F.to_pil_image(other_mask.to(torch.uint8))
        else:
            other_mask = None

        # Update paste_target to just categories of interest
        if len(idx_to_keep) > 0:
            for key, item in paste_target.items():
                if torch.is_tensor(item):
                    paste_target[key] = paste_target[key][idx_to_keep]
                else:
                    paste_target[key] = [paste_target[key][i] for i in idx_to_keep]
        else:
            paste_target = {'boxes': torch.Tensor(), 'labels': torch.Tensor()}

        return paste_image, paste_target, other_mask


def random_crop(image, target, other_mask=None,
        crop_range=[0.3, 1.0]
):
    min_scale = min(crop_range)
    max_scale = max(crop_range)
    _, orig_h, orig_w = F.get_dimensions(image)
    i = 0
    while True:
        # check the aspect ratio limitations
        i += 1
        r = min_scale + (max_scale - min_scale) * torch.rand(1)
        new_w = int(orig_w * r/ 4) * 4
        new_h = int(orig_h * r / 4) * 4


        # check for 0 area crops
        r = torch.rand(2)
        left = int((orig_w - new_w) * r[0])
        top = int((orig_h - new_h) * r[1])
        right = left + new_w
        bottom = top + new_h
        if left == right or top == bottom:
            continue

        # check for any valid boxes with centers within the crop area
        if "boxes" in target and target["boxes"].shape[0] > 0:
            cx = 0.5 * (target["boxes"][:, 0] + target["boxes"][:, 2])
            cy = 0.5 * (target["boxes"][:, 1] + target["boxes"][:, 3])
            is_within_crop_area = (left < cx) & (cx < right) & (top < cy) & (cy < bottom)
            if not is_within_crop_area.any():
                continue

            # keep only valid boxes and perform cropping
            box_copy = target["boxes"][is_within_crop_area].detach().clone()
            old_box_area = box_area(box_copy)
            box_copy[:, 0::2] -= left
            box_copy[:, 1::2] -= top
            box_copy[:, 0::2].clamp_(min=0, max=new_w)
            box_copy[:, 1::2].clamp_(min=0, max=new_h)
            new_box_area = box_area(box_copy)
            if max(new_box_area/old_box_area) < 0.8:
                continue
            target["boxes"] = box_copy
            target["labels"] = target["labels"][is_within_crop_area]


        image = F.crop(image, top, left, new_h, new_w)
        if "masks" in target:
            target["masks"] = F.crop(target["masks"], top, left, new_h, new_w)
        if other_mask is not None:
            other_mask = F.crop(other_mask, top, left, new_h, new_w)
        #print(i, new_w, new_h, left, top)
        return image, target, other_mask

def box_area(boxes):
    return (boxes[:,2] - boxes[:,0])*(boxes[:,3]-boxes[:,1])
