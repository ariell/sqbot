import torch

from .syntheticcopypaste import SyntheticCopyPaste
from .compose import Compose
from . import transforms as T
import albumentations as A


class DetectionPresetTrain:
    def __init__(self, *, data_augmentation, hflip_prob=0.5, mean=(123.0, 117.0, 104.0), shortest_side_dict={},
                 synthetic_paths=["./"], copypaste_dataset={"folder": "squidle_handfish_pretrain", "split": "train", "type": "coco"},
                 data_path="./outputs",):
        if data_augmentation == "hflip":
            self.transforms = Compose(
                [
                    T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "horvflip":
            self.transforms = Compose(
                [
                    T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdnotcp":
            self.transforms = Compose(
                [
                    #T.SyntheticCopyPaste(p=hflip_prob, synthetic_path=synthetic_paths),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                                             p=hflip_prob),
                    T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdsp3orig":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.3, synthetic_path=synthetic_paths),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                                             p=hflip_prob),
                    T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdadvcpv2":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.8, copypaste_dataset=copypaste_dataset, data_path=data_path, blending=False),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                                             p=hflip_prob),
                    T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdadvcpv4":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.5, copypaste_dataset=copypaste_dataset, data_path=data_path, blending=False,
                                       random_shift=True),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                                             p=hflip_prob),
                    T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(p=0.01),
                    T.ConvertImageDtype(torch.float),
                ]
            )

        elif data_augmentation == "hdadvcpv5":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.5, copypaste_dataset=copypaste_dataset, data_path=data_path, blending=True,
                                       random_shift=True),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    # T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                    #                         p=hflip_prob),
                    # T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(p=0.01),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdadvcpv5a":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.5, copypaste_dataset=copypaste_dataset, data_path=data_path,
                                       blending=True,
                                       random_shift=True,
                                       crop_range=[0.7, 1.0]),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    #T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                    #                         p=hflip_prob),
                    #T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(p=0.01),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdadvcpv6":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.5, copypaste_dataset=copypaste_dataset, data_path=data_path, blending=True,
                                       random_shift=True, bbox_cp=True),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    # T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                    #                         p=hflip_prob),
                    # T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(p=0.01),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdadvcpv7":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=0.5, copypaste_dataset=copypaste_dataset, data_path=data_path, blending=True,
                                       random_shift=True, bbox_cp=True, paste_image_kept_p=0.0),
                    # T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    # T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15,
                    #                         p=hflip_prob),
                    # T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(p=0.01),
                    T.ConvertImageDtype(torch.float),
                ]
            )

        elif data_augmentation == "hdadvcp":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=hflip_prob, synthetic_path=synthetic_paths),
                    #T.SelectiveScaleShortestSize(shortest_side_dict=shortest_side_dict),
                    T.RandomRotationRGBShift(limit=45, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15, p=hflip_prob),
                    T.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2, p=hflip_prob),
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensorWithSave(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "hdcponly":
            self.transforms = Compose(
                [
                    SyntheticCopyPaste(p=1.0, random_shift=True, bbox_cp=True, paste_image_kept_p=0.5),
                    T.PILToTensorWithSave(p=0.9),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "uda":
            self.transforms = Compose(
                [
                    T.RandomVerticalFlip(p=hflip_prob),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.RandomRotationRGBShift(limit=5, r_shift_limit=15, g_shift_limit=15, b_shift_limit=15, p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "lsj":
            self.transforms = Compose(
                [
                    T.ScaleJitter(target_size=(1024, 1024)),
                    T.FixedSizeCrop(size=(1024, 1024), fill=mean),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "multiscale":
            self.transforms = Compose(
                [
                    T.RandomShortestSize(
                        min_size=(480, 512, 544, 576, 608, 640, 672, 704, 736, 768, 800), max_size=1333
                    ),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "ssd":
            self.transforms = Compose(
                [
                    T.RandomPhotometricDistort(),
                    T.RandomZoomOut(fill=list(mean)),
                    T.RandomIoUCrop(),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        elif data_augmentation == "ssdlite":
            self.transforms = Compose(
                [
                    T.RandomIoUCrop(),
                    T.RandomHorizontalFlip(p=hflip_prob),
                    T.PILToTensor(),
                    T.ConvertImageDtype(torch.float),
                ]
            )
        else:
            raise ValueError(f'Unknown data augmentation policy "{data_augmentation}"')

    def __call__(self, img, target):
        return self.transforms(img, target)


class DetectionPresetEval:
    def __init__(self):
        self.transforms = Compose(
            [
                T.PILToTensor(),
                T.ConvertImageDtype(torch.float),
            ]
        )

    def __call__(self, img, target):
        return self.transforms(img, target)