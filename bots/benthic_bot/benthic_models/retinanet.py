from typing import Optional, Any

from torchvision.models import ResNet50_Weights
from torchvision.models.detection.retinanet import retinanet_resnet50_fpn, RetinaNet_ResNet50_FPN_Weights


def retinanet_resnet50_fpn_backbone(
        *,
        weights: Optional[RetinaNet_ResNet50_FPN_Weights] = None,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any):
    return retinanet_resnet50_fpn(
        weights=weights,
        progress=True,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        kwargs=kwargs,
    )
