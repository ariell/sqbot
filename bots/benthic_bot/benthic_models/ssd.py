from typing import Optional, Any

import torch.nn as nn
from torch import nn
from torchvision.models import ResNet50_Weights, resnet50
from torchvision.models.detection.ssd import ssd300_vgg16
from torchvision.models import ResNet50_Weights, resnet50, vgg16, VGG16_Weights
from torchvision.models.detection.backbone_utils import _validate_trainable_layers, _resnet_fpn_extractor
from torchvision.models.detection.faster_rcnn import AnchorGenerator
from torchvision.ops.misc import FrozenBatchNorm2d
from torchvision.models._utils import _ovewrite_value_param

def ssd_vgg16_backbone(
        *,
        weights = None,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any):

    return ssd300_vgg16(
        weights=weights,
        progress=True,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        kwargs=kwargs,
    )
