from typing import Any, Dict, List, Optional, Tuple

import torch
import torch.nn.functional as F
#from torchvision.ops import sigmoid_focal_loss

import torch.nn as nn
from torch import nn
from torchvision.models import ResNet50_Weights, resnet50, vgg16, VGG16_Weights
from torchvision.models.detection import FasterRCNN_ResNet50_FPN_Weights, FasterRCNN_ResNet50_FPN_V2_Weights, FasterRCNN
from torchvision.models.detection.backbone_utils import _validate_trainable_layers, _resnet_fpn_extractor, \
    IntermediateLayerGetter, BackboneWithFPN
from torchvision.models.detection.faster_rcnn import _default_anchorgen, FastRCNNConvFCHead, AnchorGenerator
from torchvision.models.detection.rpn import RPNHead
from torchvision.ops import MultiScaleRoIAlign
from torchvision.ops.misc import FrozenBatchNorm2d
from torchvision.models.detection.roi_heads import RoIHeads

from .focal_loss import fg_focal_loss
from .multi_class_focal_loss import multi_class_focal_loss

"""Methods to create a FasterRCNN model with a specified backbone model.  
Based on pytorch vision code.  Can specify num_classes, weights (for FasterRCNN model), 
weights_backbone (pretrained backbone) and which layers to train starting at end."""


def fasterrcnn_resnet50_fpn_v2_backbone(
        *,
        weights: Optional[FasterRCNN_ResNet50_FPN_V2_Weights] = None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
) -> FasterRCNN:
    """
    Constructs an improved Faster R-CNN model with a ResNet-50-FPN backbone from `Benchmarking Detection
    Transfer Learning with Vision Transformers <https://arxiv.org/abs/2111.11429>`__ paper.

    .. betastatus:: detection module

    It works similarly to Faster R-CNN with ResNet-50 FPN backbone. See
    :func:`~torchvision.models.detection.fasterrcnn_resnet50_fpn` for more
    details.

    Args:
        weights (:class:`~torchvision.models.detection.FasterRCNN_ResNet50_FPN_V2_Weights`, optional): The
            pretrained weights to use. See
            :class:`~torchvision.models.detection.FasterRCNN_ResNet50_FPN_V2_Weights` below for
            more details, and possible values. By default, no pre-trained
            weights are used.
        progress (bool, optional): If True, displays a progress bar of the
            download to stderr. Default is True.
        num_classes (int, optional): number of output classes of the model (including the background)
        weights_backbone (:class:`~torchvision.models.ResNet50_Weights`, optional): The
            pretrained weights for the backbone.
        trainable_backbone_layers (int, optional): number of trainable (not frozen) layers starting from
            final block. Valid values are between 0 and 5, with 5 meaning all backbone layers are
            trainable. If ``None`` is passed (the default) this value is set to 3.
        **kwargs: parameters passed to the ``torchvision.models.detection.faster_rcnn.FasterRCNN``
            base class. Please refer to the `source code
            <https://github.com/pytorch/vision/blob/main/torchvision/models/detection/faster_rcnn.py>`_
            for more details about this class.

    .. autoclass:: torchvision.models.detection.FasterRCNN_ResNet50_FPN_V2_Weights
        :members:
    """
    if isinstance(weights, str):
        weights = eval(weights)
    if isinstance(weights_backbone, str):
        weights_backbone = eval(weights_backbone)
    weights = None if weights is None else weights.verify(weights)
    weights_backbone = ResNet50_Weights.verify(weights_backbone)

    if weights is not None:
        weights_backbone = None
        if num_classes < len(weights.meta["categories"]):
            # Delete end weights
            weights_dict = weights.get_state_dict(progress=progress)
            keys_to_remove = ['roi_heads.box_predictor.cls_score.weight', 'roi_heads.box_predictor.cls_score.bias',
                              'roi_heads.box_predictor.bbox_pred.weight', 'roi_heads.box_predictor.bbox_pred.bias']
            for k in keys_to_remove:
                del weights_dict[k]
        # num_classes = _ovewrite_value_param("num_classes", num_classes, len(weights.meta["categories"]))
    elif num_classes is None:
        num_classes = 91

    is_trained = weights is not None or weights_backbone is not None
    trainable_backbone_layers = _validate_trainable_layers(is_trained, trainable_backbone_layers, 5, 3)

    backbone = resnet50(weights=weights_backbone, progress=progress)
    backbone = _resnet_fpn_extractor(backbone, trainable_backbone_layers, norm_layer=nn.BatchNorm2d)
    rpn_anchor_generator = _default_anchorgen()
    rpn_head = RPNHead(backbone.out_channels, rpn_anchor_generator.num_anchors_per_location()[0], conv_depth=2)
    box_head = FastRCNNConvFCHead(
        (backbone.out_channels, 7, 7), [256, 256, 256, 256], [1024], norm_layer=nn.BatchNorm2d
    )
    model = FasterRCNN(
        backbone=backbone,
        num_classes=num_classes,
        rpn_anchor_generator=rpn_anchor_generator,
        rpn_head=rpn_head,
        box_head=box_head,
        **kwargs,
    )

    if weights is not None:
        model.load_state_dict(weights_dict, strict=False)
    return model


def fasterrcnn_resnet50_fpn_backbone(
        *,
        weights: Optional[FasterRCNN_ResNet50_FPN_V2_Weights] = None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
) -> FasterRCNN:
    return fasterrcnn_resnet50_features(
        with_fpn=True,
        weights=weights,
        progress=progress,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        **kwargs)


def fasterrcnn_resnet50_backbone(
        *,
        weights: Optional[FasterRCNN_ResNet50_FPN_V2_Weights] = None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
) -> FasterRCNN:
    return fasterrcnn_resnet50_features(
        with_fpn=False,
        weights=weights,
        progress=progress,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        **kwargs)


def fasterrcnn_resnet50_features(
        *,
        with_fpn: bool = False,
        weights: Optional[FasterRCNN_ResNet50_FPN_V2_Weights] = None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
) -> FasterRCNN:
    """
    Faster R-CNN model with a ResNet-50-FPN backbone from the `Faster R-CNN: Towards Real-Time Object
    Detection with Region Proposal Networks <https://arxiv.org/abs/1506.01497>`__
    paper.

    .. betastatus:: detection module

    The input to the model is expected to be a list of tensors, each of shape ``[C, H, W]``, one for each
    image, and should be in ``0-1`` range. Different images can have different sizes.

    The behavior of the model changes depending on if it is in training or evaluation mode.

    During training, the model expects both the input tensors and a targets (list of dictionary),
    containing:

        - boxes (``FloatTensor[N, 4]``): the ground-truth boxes in ``[x1, y1, x2, y2]`` format, with
          ``0 <= x1 < x2 <= W`` and ``0 <= y1 < y2 <= H``.
        - labels (``Int64Tensor[N]``): the class label for each ground-truth box

    The model returns a ``Dict[Tensor]`` during training, containing the classification and regression
    losses for both the RPN and the R-CNN.

    During inference, the model requires only the input tensors, and returns the post-processed
    predictions as a ``List[Dict[Tensor]]``, one for each input image. The fields of the ``Dict`` are as
    follows, where ``N`` is the number of detections:

        - boxes (``FloatTensor[N, 4]``): the predicted boxes in ``[x1, y1, x2, y2]`` format, with
          ``0 <= x1 < x2 <= W`` and ``0 <= y1 < y2 <= H``.
        - labels (``Int64Tensor[N]``): the predicted labels for each detection
        - scores (``Tensor[N]``): the scores of each detection

    For more details on the output, you may refer to :ref:`instance_seg_output`.

    Faster R-CNN is exportable to ONNX for a fixed batch size with inputs images of fixed size.    """
    if isinstance(weights, str):
        weights = eval(weights)
    if isinstance(weights_backbone, str):
        weights_backbone = eval(weights_backbone)
    weights = None if weights is None else weights.verify(weights)
    weights_backbone = ResNet50_Weights.verify(weights_backbone)

    if weights is not None:
        weights_backbone = None
        if num_classes < len(weights.meta["categories"]):
            # Delete end weights
            weights_dict = weights.get_state_dict(progress=progress)
            keys_to_remove = ['roi_heads.box_predictor.cls_score.weight', 'roi_heads.box_predictor.cls_score.bias',
                              'roi_heads.box_predictor.bbox_pred.weight', 'roi_heads.box_predictor.bbox_pred.bias']
            for k in keys_to_remove:
                del weights_dict[k]
        # num_classes = _ovewrite_value_param("num_classes", num_classes, len(weights.meta["categories"]))
    elif num_classes is None:
        num_classes = 91

    is_trained = weights is not None or weights_backbone is not None
    trainable_backbone_layers = _validate_trainable_layers(is_trained, trainable_backbone_layers, 5, 3)
    norm_layer = FrozenBatchNorm2d if is_trained else nn.BatchNorm2d

    backbone = resnet50(weights=weights_backbone, progress=progress, norm_layer=norm_layer)
    if with_fpn:
        backbone = _resnet_fpn_extractor(backbone, trainable_backbone_layers)
        model = FasterRCNN(backbone, num_classes=num_classes, **kwargs)
    else:
        # select layers that won't be frozen
        trainable_layers = trainable_backbone_layers
        if trainable_layers < 0 or trainable_layers > 5:
            raise ValueError(f"Trainable layers should be in the range [0,5], got {trainable_layers}")
        layers_to_train = ["layer4", "layer3", "layer2", "layer1", "conv1"][:trainable_layers]
        if trainable_layers == 5:
            layers_to_train.append("bn1")
        for name, parameter in backbone.named_parameters():
            if all([not name.startswith(layer) for layer in layers_to_train]):
                parameter.requires_grad_(False)

        returned_layers = [4]
        return_layers = {f"layer{k}": str(v) for v, k in enumerate(returned_layers)}
        backbone = IntermediateLayerGetter(backbone, return_layers=return_layers)
        backbone.out_channels = 2048
        roi_pooler = MultiScaleRoIAlign(featmap_names=["3"],
                                        output_size=7,
                                        sampling_ratio=2)
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))
        model = FasterRCNN(backbone, num_classes=num_classes, roi_pooler=roi_pooler,
                           rpn_anchor_generator=anchor_generator,
                           **kwargs)
    if "focal_loss" in kwargs and kwargs["focal_loss"]:
        roi_heads = FocalLossRoIHeads(model.roi_heads)
        model.roi_heads = roi_heads

    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
        if weights == FasterRCNN_ResNet50_FPN_Weights.COCO_V1:
            overwrite_eps(model, 0.0)

    return model


def fasterrcnn_vgg16_backbone(
        *,
        weights=None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[VGG16_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
):
    return fasterrcnn_vgg16_backbone_features(
        with_fpn=False,
        weights=weights,
        progress=progress,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        **kwargs)


def fasterrcnn_vgg16_fpn_backbone(
        *,
        weights=None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[VGG16_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
):
    return fasterrcnn_vgg16_backbone_features(
        with_fpn=True,
        weights=weights,
        progress=progress,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        **kwargs)


def fasterrcnn_vgg16_backbone_features(
        *,
        with_fpn=False,
        weights=None,
        progress: bool = True,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[VGG16_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any,
) -> FasterRCNN:
    """
    """
    ## Load backbone
    if isinstance(weights_backbone, str):
        weights_backbone = eval(weights_backbone)
    if with_fpn:
        # Based on https://discuss.pytorch.org/t/fpn-with-vgg16-backbone-for-fasterrcnn/163166/2
        vgg = VGG16_features(weights_backbone=weights_backbone, progress=progress,
                             trainable_backbone_layers=trainable_backbone_layers)
        in_channels_list = [128, 256, 512, 512]
        return_layers = {'layer_1': '0', 'layer_2': '1', 'layer_3': '2', 'layer_4': '3'}
        out_channels = 256
        backbone = BackboneWithFPN(vgg, return_layers, in_channels_list, out_channels)
        backbone.out_channels = 256
        anchor_generator = AnchorGenerator(
            sizes=((32, 64, 128, 256, 512), (32, 64, 128, 256, 512), (32, 64, 128, 256, 512), (32, 64, 128, 256, 512)),
            aspect_ratios=((0.5, 1.0, 2.0), (0.5, 1.0, 2.0), (0.5, 1.0, 2.0), (0.5, 1.0, 2.0)))
        roi_pooler = MultiScaleRoIAlign(featmap_names=["2"], output_size=7, sampling_ratio=2)
        model = FasterRCNN(backbone, #roi_pooler=roi_pooler, rpn_anchor_generator=anchor_generator,
                           num_classes=num_classes, **kwargs)
    else:
        vgg = vgg16(progress=progress, weights=weights_backbone)

        # Freeze layers
        if trainable_backbone_layers is None:
            trainable_backbone_layers = len(vgg.features)
        freeze_before = len(vgg.features) if trainable_backbone_layers == 0 else len(
            vgg.features) - trainable_backbone_layers

        for layer in vgg.features[:freeze_before]:
            for p in layer.parameters():
                p.requires_grad = False
        backbone = nn.Sequential(*list(vgg.features)[:-1])

        backbone.out_channels = 512  # Output channels for VGG16

        roi_pooler = MultiScaleRoIAlign(featmap_names=['0'],
                                        output_size=7,
                                        sampling_ratio=2)
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))
        model = FasterRCNN(backbone, num_classes=num_classes, roi_pooler=roi_pooler,
                           rpn_anchor_generator=anchor_generator,
                           **kwargs)

    return model


class VGG16_features(nn.Module):
    """'vgg16':            ('block5_conv3', 'block4_conv3', 'block3_conv3')
    From https://github.com/MrGiovanni/UNetPlusPlus"""
    def __init__(self, weights_backbone=None, progress=True, trainable_backbone_layers=None):
        super().__init__()
        if isinstance(weights_backbone, str):
            weights_backbone = eval(weights_backbone)
        vgg = vgg16(progress=progress, weights=weights_backbone)
        # Freeze layers before splitting up
        if trainable_backbone_layers is None:
            trainable_backbone_layers = len(vgg.features)
        freeze_before = len(vgg.features) if trainable_backbone_layers == 0 else len(
            vgg.features) - trainable_backbone_layers
        for layer in vgg.features[:freeze_before]:
            for p in layer.parameters():
                p.requires_grad = False

        # Split layers
        # Based on https://github.com/Hanqer/deep-hough-transform(Need to add 1 when segmenting array)
        self.layer_1 = nn.Sequential(vgg.features[:9])
        self.layer_2 = nn.Sequential(vgg.features[9:16])
        self.layer_3 = nn.Sequential(vgg.features[16:23])
        self.layer_4 = nn.Sequential(vgg.features[23:-1])

    def forward(self, x):
        x = self.layer_1(x)
        x = self.layer_2(x)
        x = self.layer_3(x)
        x = self.layer_4(x)
        x = self.layer_5(x)
        return x


def overwrite_eps(model: nn.Module, eps: float) -> None:
    """
    This method overwrites the default eps values of all the
    FrozenBatchNorm2d layers of the model with the provided value.
    This is necessary to address the BC-breaking change introduced
    by the bug-fix at pytorch/vision#2933. The overwrite is applied
    only when the pretrained weights are loaded to maintain compatibility
    with previous versions.

    Args:
        model (nn.Module): The model on which we perform the overwrite.
        eps (float): The new value of eps.
    """
    for module in model.modules():
        if isinstance(module, FrozenBatchNorm2d):
            module.eps = eps


def fastrcnn_focal_loss(class_logits, box_regression, labels, regression_targets):
    # type: (Tensor, Tensor, List[Tensor], List[Tensor]) -> Tuple[Tensor, Tensor]
    """
    Computes the loss for Faster R-CNN.

    Args:
        class_logits (Tensor)
        box_regression (Tensor)
        labels (list[BoxList])
        regression_targets (Tensor)

    Returns:
        classification_loss (Tensor)
        box_loss (Tensor)
    """

    labels = torch.cat(labels, dim=0)
    regression_targets = torch.cat(regression_targets, dim=0)

    #classification_loss = F.cross_entropy(class_logits, labels)
    classification_loss = multi_class_focal_loss(gamma=2, alpha=[0.5, 5.0], device=class_logits.device)(class_logits, labels)
    #focal_loss = fg_focal_loss(class_logits, labels, reduction="mean", alpha=0)
    focal_loss = 0

    # get indices that correspond to the regression targets for
    # the corresponding ground truth labels, to be used with
    # advanced indexing
    sampled_pos_inds_subset = torch.where(labels > 0)[0]
    labels_pos = labels[sampled_pos_inds_subset]
    N, num_classes = class_logits.shape
    box_regression = box_regression.reshape(N, box_regression.size(-1) // 4, 4)

    box_loss = F.smooth_l1_loss(
        box_regression[sampled_pos_inds_subset, labels_pos],
        regression_targets[sampled_pos_inds_subset],
        beta=1 / 9,
        reduction="sum",
    )
    box_loss = box_loss / labels.numel()

    return classification_loss, box_loss, focal_loss

class FocalLossRoIHeads(RoIHeads):
    def __init__(
        self,
        roi_heads
    ):
        nn.Module.__init__(self)

        self.box_similarity = roi_heads.box_similarity
        # assign ground-truth boxes for each proposal
        self.proposal_matcher = roi_heads.proposal_matcher

        self.fg_bg_sampler = roi_heads.fg_bg_sampler

        self.box_coder = roi_heads.box_coder
        self.box_roi_pool = roi_heads.box_roi_pool
        self.box_head = roi_heads.box_head
        self.box_predictor = roi_heads.box_predictor

        self.score_thresh = roi_heads.score_thresh
        self.nms_thresh = roi_heads.nms_thresh
        self.detections_per_img = roi_heads.detections_per_img

        self.mask_roi_pool = roi_heads.mask_roi_pool
        self.mask_head = roi_heads.mask_head
        self.mask_predictor = roi_heads.mask_predictor

        self.keypoint_roi_pool = roi_heads.keypoint_roi_pool
        self.keypoint_head = roi_heads.keypoint_head
        self.keypoint_predictor = roi_heads.keypoint_predictor


    def forward(
        self,
        features,  # type: Dict[str, Tensor]
        proposals,  # type: List[Tensor]
        image_shapes,  # type: List[Tuple[int, int]]
        targets=None,  # type: Optional[List[Dict[str, Tensor]]]
    ):
        # type: (...) -> Tuple[List[Dict[str, Tensor]], Dict[str, Tensor]]
        """
        Args:
            features (List[Tensor])
            proposals (List[Tensor[N, 4]])
            image_shapes (List[Tuple[H, W]])
            targets (List[Dict])
        """
        if targets is not None:
            for t in targets:
                # TODO: https://github.com/pytorch/pytorch/issues/26731
                floating_point_types = (torch.float, torch.double, torch.half)
                if not t["boxes"].dtype in floating_point_types:
                    raise TypeError(f"target boxes must of float type, instead got {t['boxes'].dtype}")
                if not t["labels"].dtype == torch.int64:
                    raise TypeError("target labels must of int64 type, instead got {t['labels'].dtype}")
                if self.has_keypoint():
                    if not t["keypoints"].dtype == torch.float32:
                        raise TypeError(f"target keypoints must of float type, instead got {t['keypoints'].dtype}")

        if self.training:
            proposals, matched_idxs, labels, regression_targets = self.select_training_samples(proposals, targets)
        else:
            labels = None
            regression_targets = None
            matched_idxs = None

        box_features = self.box_roi_pool(features, proposals, image_shapes)
        box_features = self.box_head(box_features)
        class_logits, box_regression = self.box_predictor(box_features)

        result: List[Dict[str, torch.Tensor]] = []
        losses = {}
        if self.training:
            if labels is None:
                raise ValueError("labels cannot be None")
            if regression_targets is None:
                raise ValueError("regression_targets cannot be None")
            loss_classifier, loss_box_reg, loss_fg_focal_loss = fastrcnn_focal_loss(class_logits, box_regression, labels, regression_targets)
            losses = {"loss_classifier": loss_classifier, "loss_box_reg": loss_box_reg, "loss_fg_focal_loss": loss_fg_focal_loss}
        else:
            boxes, scores, labels = self.postprocess_detections(class_logits, box_regression, proposals, image_shapes)
            num_images = len(boxes)
            for i in range(num_images):
                result.append(
                    {
                        "boxes": boxes[i],
                        "labels": labels[i],
                        "scores": scores[i],
                    }
                )

        if self.has_mask():
            mask_proposals = [p["boxes"] for p in result]
            if self.training:
                if matched_idxs is None:
                    raise ValueError("if in trainning, matched_idxs should not be None")

                # during training, only focus on positive boxes
                num_images = len(proposals)
                mask_proposals = []
                pos_matched_idxs = []
                for img_id in range(num_images):
                    pos = torch.where(labels[img_id] > 0)[0]
                    mask_proposals.append(proposals[img_id][pos])
                    pos_matched_idxs.append(matched_idxs[img_id][pos])
            else:
                pos_matched_idxs = None

            if self.mask_roi_pool is not None:
                mask_features = self.mask_roi_pool(features, mask_proposals, image_shapes)
                mask_features = self.mask_head(mask_features)
                mask_logits = self.mask_predictor(mask_features)
            else:
                raise Exception("Expected mask_roi_pool to be not None")

            loss_mask = {}
            if self.training:
                if targets is None or pos_matched_idxs is None or mask_logits is None:
                    raise ValueError("targets, pos_matched_idxs, mask_logits cannot be None when training")

                gt_masks = [t["masks"] for t in targets]
                gt_labels = [t["labels"] for t in targets]
                rcnn_loss_mask = maskrcnn_loss(mask_logits, mask_proposals, gt_masks, gt_labels, pos_matched_idxs)
                loss_mask = {"loss_mask": rcnn_loss_mask}
            else:
                labels = [r["labels"] for r in result]
                masks_probs = maskrcnn_inference(mask_logits, labels)
                for mask_prob, r in zip(masks_probs, result):
                    r["masks"] = mask_prob

            losses.update(loss_mask)

        # keep none checks in if conditional so torchscript will conditionally
        # compile each branch
        if (
            self.keypoint_roi_pool is not None
            and self.keypoint_head is not None
            and self.keypoint_predictor is not None
        ):
            keypoint_proposals = [p["boxes"] for p in result]
            if self.training:
                # during training, only focus on positive boxes
                num_images = len(proposals)
                keypoint_proposals = []
                pos_matched_idxs = []
                if matched_idxs is None:
                    raise ValueError("if in trainning, matched_idxs should not be None")

                for img_id in range(num_images):
                    pos = torch.where(labels[img_id] > 0)[0]
                    keypoint_proposals.append(proposals[img_id][pos])
                    pos_matched_idxs.append(matched_idxs[img_id][pos])
            else:
                pos_matched_idxs = None

            keypoint_features = self.keypoint_roi_pool(features, keypoint_proposals, image_shapes)
            keypoint_features = self.keypoint_head(keypoint_features)
            keypoint_logits = self.keypoint_predictor(keypoint_features)

            loss_keypoint = {}
            if self.training:
                if targets is None or pos_matched_idxs is None:
                    raise ValueError("both targets and pos_matched_idxs should not be None when in training mode")

                gt_keypoints = [t["keypoints"] for t in targets]
                rcnn_loss_keypoint = keypointrcnn_loss(
                    keypoint_logits, keypoint_proposals, gt_keypoints, pos_matched_idxs
                )
                loss_keypoint = {"loss_keypoint": rcnn_loss_keypoint}
            else:
                if keypoint_logits is None or keypoint_proposals is None:
                    raise ValueError(
                        "both keypoint_logits and keypoint_proposals should not be None when not in training mode"
                    )

                keypoints_probs, kp_scores = keypointrcnn_inference(keypoint_logits, keypoint_proposals)
                for keypoint_prob, kps, r in zip(keypoints_probs, kp_scores, result):
                    r["keypoints"] = keypoint_prob
                    r["keypoints_scores"] = kps
            losses.update(loss_keypoint)

        return result, losses
