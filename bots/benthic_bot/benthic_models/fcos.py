from typing import Optional, Any

import torch.nn as nn
from torch import nn
from torchvision.models import ResNet50_Weights, resnet50
from torchvision.models.detection import FCOS, FCOS_ResNet50_FPN_Weights, fcos_resnet50_fpn
from torchvision.models.detection import FasterRCNN_ResNet50_FPN_V2_Weights, FasterRCNN
from torchvision.models import ResNet50_Weights, resnet50, vgg16, VGG16_Weights
from torchvision.models.detection.backbone_utils import _validate_trainable_layers, _resnet_fpn_extractor
from torchvision.models.detection.faster_rcnn import AnchorGenerator
from torchvision.ops.misc import FrozenBatchNorm2d
from torchvision.models._utils import _ovewrite_value_param

def fcos_resnet50_fpn_backbone(
        *,
        weights: Optional[FCOS_ResNet50_FPN_Weights] = None,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any):

    return fcos_resnet50_fpn(
        weights=weights,
        progress=True,
        num_classes=num_classes,
        weights_backbone=weights_backbone,
        trainable_backbone_layers=trainable_backbone_layers,
        kwargs=kwargs,
        )

def fcos_vgg16_backbone(
        *,
        weights: Optional[FasterRCNN_ResNet50_FPN_V2_Weights] = None,
        num_classes: Optional[int] = None,
        weights_backbone: Optional[ResNet50_Weights] = None,
        trainable_backbone_layers: Optional[int] = None,
        progress=True,
        **kwargs: Any):

    if weights is not None:
        weights_backbone = None
        num_classes = _ovewrite_value_param("num_classes", num_classes, len(weights.meta["categories"]))
    elif num_classes is None:
        num_classes = 91

    if weights_backbone is not None:
        vgg = vgg16(progress=progress, weights=weights_backbone)

        # Freeze layers
        if trainable_backbone_layers is None:
            trainable_backbone_layers = len(vgg.features)
        freeze_before = len(vgg.features) if trainable_backbone_layers == 0 else len(
            vgg.features) - trainable_backbone_layers

        for layer in vgg.features[:freeze_before]:
            for p in layer.parameters():
                p.requires_grad = False
        backbone = nn.Sequential(*list(vgg.features)[:-1])

        backbone.out_channels = 512  # Output channels for VGG16


    # let's make the network generate 5 x 3 anchors per spatial
    # location, with 5 different sizes and 3 different aspect
    # ratios. We have a Tuple[Tuple[int]] because each feature
    # map could potentially have different sizes and
    # aspect ratios
    anchor_generator = AnchorGenerator(
        sizes=((8,), (16,), (32,), (64,), (128,)),
        aspect_ratios=((1.0,),)
    )

    model = FCOS(
        backbone,
        num_classes=num_classes,
        anchor_generator=anchor_generator,
        **kwargs)

    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress))

    return model

def overwrite_eps(model: nn.Module, eps: float) -> None:
    """
    This method overwrites the default eps values of all the
    FrozenBatchNorm2d layers of the model with the provided value.
    This is necessary to address the BC-breaking change introduced
    by the bug-fix at pytorch/vision#2933. The overwrite is applied
    only when the pretrained weights are loaded to maintain compatibility
    with previous versions.

    Args:
        model (nn.Module): The model on which we perform the overwrite.
        eps (float): The new value of eps.
    """
    for module in model.modules():
        if isinstance(module, FrozenBatchNorm2d):
            module.eps = eps
