import os.path

import torch

from .fasterrcnn import *
from .fcos import *
from .retinanet import *
from .ssd import *
#from .mmdet_models import *
from .yolo import *
from ensemble_boxes import *

def get_od_model(opt):
    # load network
    print("Creating model")
    model_fn = opt.train.model
    if model_fn.startswith('ensemble'):
        return EnsembleOD(opt)
    kwargs = {"trainable_backbone_layers": opt.train.trainable_backbone_layers,
              'num_classes': opt.train.num_classes,
              'weights': opt.train.weights,
              'weights_backbone': opt.train.weights_backbone,
              }
    if opt.train.data_augmentation in ["multiscale", "lsj"]:
        kwargs["_skip_resize"] = True
    if opt.train.rpn_score_thresh is not None:
        kwargs["rpn_score_thresh"] = opt.train.rpn_score_thresh
    if "rcnn" in opt.train.model:
        if opt.train.rpn_score_thresh is not None:
            kwargs["rpn_score_thresh"] = opt.train.rpn_score_thresh
    if opt.train.min_size is not None:
        kwargs["min_size"] = opt.train.min_size

    if opt.train.focal_loss is not None:
        kwargs["focal_loss"] = opt.train.focal_loss

    model = eval(model_fn)(**kwargs)

    if model_fn not in ["mmdet_libra_rcnn", "yolov5s"]:

        if "min_size" in kwargs.keys():
            model.transform.min_size = (kwargs["min_size"],)

    return model

class EnsembleOD(nn.Module):
    def __init__(self, opt, iou_thresh=0.2):
        super().__init__()
        self.iou_thresh = iou_thresh
        base_model = opt.train.model[9:] #base model after ensemble_ prefix
        opt.train.model = base_model
        self.models = []
        for _ in range(len(opt.checkpoint)):
            model = get_od_model(opt)
            self.models.append(model)

    def to(self, device):
        for m in self.models:
            m.to(device)
        return None

    def eval(self):
        for m in self.models:
            m.eval()

    def forward(self, images, targets=None):
        detections = [m(images, targets) for m in self.models]
        keys = detections[0][0].keys()
        n_img = len(detections[0])
        outputs = [{k: [] for k in keys} for _ in range(n_img)]
        for model_idx in range(len(self.models)):

            for img_idx in range(n_img):
                result = detections[model_idx][img_idx]
                for k in keys:
                    outputs[img_idx][k].append(result[k])
        for img_idx in range(n_img):
            for k in keys:
                outputs[img_idx][k] = torch.cat(outputs[img_idx][k], dim=0)
            outputs_img = outputs[img_idx]

            if outputs_img['labels'].shape[0] > 0:
                # Normalise
                max_x = torch.max(outputs_img['boxes'][:,2])
                max_y = torch.max(outputs_img['boxes'][:,3])
                scaled = torch.tensor([max_x, max_y, max_x, max_y]).to(outputs_img['boxes'])
                outputs_img['boxes'] = torch.div(outputs_img['boxes'], scaled)
                boxes_list = [outputs_img['boxes'].tolist()]

                scores_list = [outputs_img['scores'].tolist()]
                labels_list = [outputs_img['labels'].tolist()]
                boxes, scores, labels = nms(boxes_list, scores_list, labels_list, iou_thr=self.iou_thresh)

                outputs_img['scores'] = torch.tensor(scores)
                outputs_img['boxes'] = torch.mul(torch.tensor(boxes), scaled.to('cpu'))
                outputs_img['labels'] = torch.tensor(labels)

        return outputs


def load_checkpoint_for_model(model, checkpoints):
    if isinstance(model, EnsembleOD):
        for i, checkpoint in enumerate(checkpoints):
            checkpoint = torch.load(os.path.expanduser(checkpoint), map_location="cpu")
            checkpoint["model"] = {key.replace("ema_model.", ""): value for key, value in checkpoint["model"].items()}
            model.models[i].load_state_dict(checkpoint['model'])
            print(f"Loading checkpoint from last saved epoch {checkpoint['epoch']}")
    else:
        checkpoint = torch.load(os.path.expanduser(checkpoints), map_location="cpu")
        checkpoint["model"] = {key.replace("ema_model.", ""): value for key, value in checkpoint["model"].items()}
        model.load_state_dict(checkpoint['model'])
        print(f"Loading checkpoint from last saved epoch {checkpoint['epoch']}")
    return checkpoint
