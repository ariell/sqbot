from typing import Optional, Any
import torch

#from ultralytics import YOLO
def yolov5s(
        *,
        weights = None,
        num_classes: Optional[int] = None,
        weights_backbone = None,
        trainable_backbone_layers: Optional[int] = None,
        **kwargs: Any):

    return torch.hub.load('ultralytics/yolov5', 'yolov5s', classes=num_classes, autoshape=False, pretrained=False)
