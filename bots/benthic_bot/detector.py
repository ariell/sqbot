import os

import cv2
import torch
from PIL import Image
from omegaconf import DictConfig
from sqapi.annotate import Annotator
from sqapi.helpers import create_parser
from sqapi.media import SQMediaObject
from sys import platform

from .benthic_datasets.utils_dataset import get_transform
from .benthic_models.get_od_model import get_od_model, load_checkpoint_for_model


# Instantiate the Trainer with specific settings
# DEVICE = "mps" if platform == "darwin" else "cuda" if torch.cuda.is_available() else "cpu"
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"


class ACFRDetectorBot(Annotator):
    def __init__(self, model_path: str, device: str = DEVICE, cache_dir=None, metadata=None, **kwargs) -> object:
        """
        Detector using Torch.
        This detector generated bounding boxes that are submitted to SQ+

        :param model_path: the path of the torch model checkpoint
        :param device:
        """
        super().__init__(**kwargs)
        self.metadata = metadata
        self.device = device
        self.cache_dir = cache_dir

        self.checkpoint_path = model_path
        self.checkpoint_with_opt = model_path
        if "," in model_path:  # Assume it's a list therefore ensemble
            model_paths = model_path.strip("\'").split(",")
            self.checkpoint_with_opt = model_paths[0]
            self.checkpoint_path = model_paths

        # Load checkpoint (or first checkpoint for ensemble) to get opt configuration.
        self.checkpoint = torch.load(os.path.expanduser(self.checkpoint_with_opt), map_location="cpu")
        self.opt = DictConfig({'train': self.checkpoint['self.opt.train']})
        # self.opt.train.num_classes = 2
        if isinstance(self.checkpoint_path, list):
            self.opt.train.model = f"ensemble_{self.opt.train.model}"
            self.opt.checkpoint = self.checkpoint_path

        self.model = get_od_model(self.opt)
        self.model.to(self.device)
        load_checkpoint_for_model(self.model, self.checkpoint_path)

    def tensor2polygon(self, box):
        # Convert prediction boxes to pixel coordinates
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])

        return [[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]]

    def get_image_name(self, url):
        img_name = os.path.split(url)
        if len(img_name[1]) == 0:
            img_name = os.path.basename(img_name[0])
        else:
            img_name = img_name[1]
        return img_name

    def process_point(self, point, mediaobj, annotation_set_id):
        return

    # def process_annotation_set(self, original_annotation_set):
    #     """
    #
    #     :param original_annotation_set:
    #     :return:
    #     """
    #     self.code2label = self.get_label_mappings(original_annotation_set)
    #     if self.non_supplementary:
    #         # Update comment on original annotation_set
    #         annotation_set = original_annotation_set
    #         result = self.sqapi.get(f"/api/annotation_set/{annotation_set['id']}")
    #         result = result.execute().json()
    #         comment = "" if result['description'] is None else result['description']
    #         comment = f"{comment}. prob_thresh: {self.prob_thresh}, checkpoint: {self.checkpoint_path}"
    #         p = {'description': comment}
    #         self.sqapi.patch(f"/api/annotation_set/{annotation_set['id']}", json_data=p).execute()
    #     else:
    #         epoch = self.checkpoint.get('epoch', "Unknown")
    #         annotation_set_name = original_annotation_set.get("name")
    #         description = f"Suggestions from ACfR benthic detector for '{annotation_set_name}'. For info email heather.doig@sydney.edu.au \n"
    #         description += f"Checkpoint from epoch {epoch}, threshold level = {self.prob_thresh}, model {self.checkpoint_path}, metadata: {self.metadata}."
    #         annotation_set = self.create_supplemental_annotation_set(parent_data=original_annotation_set,
    #                                                                  child_description=description,
    #                                                                  child_name=self.annotator_info)
    #
    #         # Get group id of parent and add to supplementary annotation set.
    #         groups = self.sqapi.get("/api/groups", page=1, results_per_page=200).filter(
    #            name="shared_annotation_sets", op="any", val=dict(name="id", op="eq", val=original_annotation_set['id'])).execute().json()
    #         if groups['num_results'] > 0:
    #            for g in groups['objects']:
    #                self.sqapi.post(f"/api/annotation_set/{annotation_set['id']}/group/{g['id']}").execute().json()
    #     media_count, point_count, annotation_count = self.annotate_media_list(annotation_set)
    #     return media_count, point_count, annotation_count

    def detect_points(self, mediaobj: SQMediaObject, comment=""):
        img = None
        if self.cache_dir is not None:
            # Check cache first.
            image_name = self.get_image_name(mediaobj.url)
            image_filepath = os.path.join(self.cache_dir, image_name)
            if os.path.exists(image_filepath):
                img = Image.open(image_filepath).convert("RGB")  # has already been padded, so will return padded image
                mediaobj.height = img.height
                mediaobj.width = img.width
        if img is None:
            if not mediaobj.is_processed:
                orig_image = mediaobj.data()
                img = mediaobj.data(
                    Image.fromarray(cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)))  # convert to PIL Image
            else:
                img = mediaobj.data()  # will return processed image

        transform = get_transform(False, self.opt.train)
        img, target = transform(img, None)
        images = list(img.to(self.device) for img in [img])

        # Use the models and visualize the prediction
        self.model.eval()
        if torch.cuda.is_available():
            torch.cuda.synchronize()
        outputs = self.model(images)
        outputs = [{k: v.to("cpu") for k, v in t.items()} for t in outputs]
        prediction = outputs[0]

        points = []
        boxes = prediction['boxes'].detach().cpu()
        for i in range(len(prediction['labels'])):
            likelihood = prediction['scores'].detach().cpu()[i]
            code = int(prediction['labels'][i])
            if likelihood > self.prob_thresh:
                polygon = self.tensor2polygon(boxes[i, :])
                p = self.create_annotation_label_point_px(
                    code, likelihood=likelihood, polygon=polygon, width=mediaobj.width, height=mediaobj.height,
                    comment=comment
                )
                points.append(p)

        return points


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(ACFRDetectorBot)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)
    parser.add_argument('--user_group_id', help="Process all annotation_sets contained in a specific user_group",
                        type=int)
    parser.add_argument('--after_date', help="Process all annotation_sets after a date YYYY-MM-DD", type=str)
    parser.add_argument('--cache-dir', help="Directory with images downloaded already", type=str, default="")

    args = parser.parse_args()
    bot = ACFRDetectorBot(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set")

    # Filter annotation sets based on ID
    if args.annotation_set_id:
        r.filter("id", "eq", args.annotation_set_id)

    # Constrain date ranges to annotation_sets ceated after a specific date
    if args.after_date:
        r.filter("created_at", "gt", args.after_date)

    # Filter annotation_sets based on a user group
    if args.user_group_id:
        r.filter(name="usergroups", op="any", val=dict(name="id", op="eq", val=args.user_group_id))

    # Only return annotation_sets that do not already have suggestions from this user
    # r.filter_not(qf("children", "any", val=qf("user_id", "eq", bot.sqapi.current_user.get("id"))))

    bot.start(r)
