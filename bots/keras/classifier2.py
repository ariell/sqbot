import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.applications import InceptionV3 
from tensorflow.keras.applications.inception_v3 import preprocess_input as preprocess_input_inception
import os
from sqapi.helpers import create_parser
from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf
import cv2
import numpy as np

CROP_RATIO = 0.2 
class TfBOT(Annotator):

    def __init__(self, model_path: str, crop_ratio: float = CROP_RATIO,patch_path: str = None, **kwargs: object) -> object:
        """
        Runs a tensorflow model
        :param model_path: the path of the tensorflow model weights
        :param crop_ratio: a factor used to crop around a point proportional to the width and height of the original image
        :param patch_path: an optional path to cache the patches (useful if doing multiple runs on the same points)
        """
        super().__init__(**kwargs)
        self.preprocess_input = preprocess_input_inception
        self.model_weights = model_path
        self.model_input_size = 299
        self.model = self.get_inception_classifier()
        self.crop_ratio = crop_ratio
        self.patch_path = patch_path
        if self.patch_path is not None:
            if not os.path.isdir(self.patch_path):
                os.makedirs(self.patch_path)
                
    def get_inception_classifier(self):
        """
        Loads the pretrained tensorflow model using model_weights 
        """
        backbone =  InceptionV3(input_shape=(self.model_input_size, self.model_input_size, 3), include_top=False, weights=None)
        backbone.trainable = False
        dense_number = 512
        dropout_ratio = 0.3
        class_num = 7
        last_activation = "softmax"
        model = tf.keras.Sequential([
            backbone,
            layers.GlobalAveragePooling2D(),
            layers.Dense(dense_number, activation='relu', name='hidden_layer_1'),
            layers.Dropout(dropout_ratio),
            layers.Dense(class_num, activation=last_activation, name='output')], name="multiclass_classifier")
        model.load_weights(self.model_weights)
        return model
    
    def crop_around_point (self,img,x,y,crop_size):
        """
        Crops around a point based on the given crop size. It also check if the given point is close to the boundries. If yes, then it crops around the the point
        as much as possible making sure that the point is still at the center of the patch
        :param img: the input image which is a numpy array
        :param x,y: point coordinate
        :param crop_size: a float number to set the crop size computed based on the crop ratio
        """
        xx, yy = crop_size//2,crop_size//2
        if x-(crop_size//2)<0 or y-(crop_size//2)<0:
            if x-(crop_size//2)<0:
                xx = min(x,y)
            if y-(crop_size//2)<0:
                yy = min(x,y)
        if x+(crop_size//2)>=img.shape[1] or y+(crop_size//2)>=img.shape[0]: 
            if x+(crop_size//2)>=img.shape[1]:
                xx = min(img.shape[1]-x,img.shape[0]-y)
            if y+(crop_size//2)>=img.shape[0]: 
                yy = min(img.shape[1]-x,img.shape[0]-y)
        xx,yy = min(xx,yy), min(xx,yy)
        x1 = x - xx
        y1 = y - yy
        x2 = x + xx
        y2 = y + yy
        cropped_image = img[y1:y2, x1:x2].copy()
        return cropped_image
    
    def get_patch(self, x, y, mediaobj):
        """
        Returns a patch around the given point coordinate
        """
        imagename = os.path.basename(mediaobj.url)
        cropfile_name = "{}_{}_{}.jpg".format(imagename, x, y)
        cropfile_path = os.path.join(self.patch_path, cropfile_name) if self.patch_path else None
        if cropfile_path is not None and os.path.isfile(cropfile_path):
            # return cached image if it exists
            cropped_frame = cv2.imread(cropfile_path)
            return cropped_frame
        else:
            frame = mediaobj.data()
            x = int(x*mediaobj.width)
            y = int(y*mediaobj.height)
            crop_size = int(((mediaobj.width+mediaobj.height)/2)*self.crop_ratio)
            cropped_frame = self.crop_around_point (frame,x,y,crop_size)
            try:
                cropped_frame = cv2.resize(cropped_frame, (self.model_input_size, self.model_input_size))
            except Exception as e:
                print(f"Cannot resize patch x:{x}, y:{y}, in {mediaobj.url}")
            if cropfile_path is not None:
                cv2.imwrite(cropfile_path, cropped_frame)
            return cropped_frame
        
    def classify_point(self, mediaobj, x, y, t):
         """ returns: classifier_code, prob """
         patch_img = self.get_patch(x, y, mediaobj)
         patch_img = cv2.cvtColor(patch_img, cv2.COLOR_BGR2RGB)
         patch_img = self.preprocess_input(patch_img.astype(np.float32))
         predictions = self.model.predict(np.expand_dims(patch_img, axis=0), verbose=1)
         predictions = predictions[0]
         top_k = predictions.argsort()[-3:][::-1]  # prediction in descending probability
         classifier_code = top_k[0]
         prob = predictions[top_k[0]]
         return classifier_code, float(prob)
    

if __name__ == '__main__':

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(TfBOT)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)

    args = parser.parse_args()
    bot = TfBOT(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class, and filter by annotation_set_id
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    # Start the bot in a loop that polls at a defined interval
    bot.start(r)
    

    




