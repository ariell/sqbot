# Keras BOT

Uses keras to run a tensorflow model

```
usage: classifier.py [-h] [--model_path MODEL_PATH] [--patch_width PATCH_WIDTH] [--patch_height PATCH_HEIGHT] 
                     [--network NETWORK] [--patch_path PATCH_PATH] [--host HOST] [--api_key API_KEY] 
                     [--annotator_info ANNOTATOR_INFO] [--prob_thresh PROB_THRESH] [--poll_delay POLL_DELAY]
                     [--label_map_file LABEL_MAP_FILE] [--verbosity VERBOSITY] [--email_results] [--non_supplementary] 
                     [--annotation_set_id ANNOTATION_SET_ID] [--user_group_id USER_GROUP_ID] 
                     [--affiliation_group_id AFFILIATION_GROUP_ID] [--after_date AFTER_DATE]
                     [--media_count_max MEDIA_COUNT_MAX]

optional arguments:
  -h, --help            show this help message and exit
  --model_path MODEL_PATH
                        the path of the tensorflow model
                        type: str  | default: None
  --patch_width PATCH_WIDTH
                        with of the patches
                        type: int  | default: 299
  --patch_height PATCH_HEIGHT
                        height of the patches
                        type: int  | default: 299
  --network NETWORK     the network to use for the model
                        type: str  | default: keras.applications.inception_v3
  --patch_path PATCH_PATH
                        an optional path to cache the patches (useful if doing multiple runs on the same points)
                        type: str  | default: None
  --host HOST           the Squidle+ instance hostname
                        type: str  | default: https://squidle.org
  --api_key API_KEY     the API key for the user on that `host`. If omitted, you'll be prompted to log in.
                        type: str  | default: None
  --annotator_info ANNOTATOR_INFO
                        used for the name of the annotation_set, include version info, defaults to ClassName
                        type: str  | default: None
  --prob_thresh PROB_THRESH
                        probability threshold for submitted labels, only submitted if p > prob_thresh
                        type: float  | default: 0.5
  --poll_delay POLL_DELAY
                        the poll delay for running the loop. To run once, set poll_delay = -1 (default)
                        type: int  | default: -1
  --label_map_file LABEL_MAP_FILE
                        path to a local file that contains the label mappings
                        type: str  | default: None
  --verbosity VERBOSITY
                        the verbosity of the output (0,1,2,3)
                        type: int  | default: 2
  --email_results       flag to optionally send an email upon completion
                        type: bool  | default: None
  --non_supplementary   flag to optionally label directly without creating a supplementary set (with suggestions)
                        type: bool  | default: False
  --annotation_set_id ANNOTATION_SET_ID
                        Process specific annotation_set
  --user_group_id USER_GROUP_ID
                        Process all annotation_sets contained in a specific user_group
  --affiliation_group_id AFFILIATION_GROUP_ID
                        Process all annotation_sets contained in a specific Affiliation
  --after_date AFTER_DATE
                        Process all annotation_sets after a date YYYY-MM-DD
  --media_count_max MEDIA_COUNT_MAX
                        Filter annotation_sets that have less than a specific number of media objects
```