import cv2
import numpy as np
from sqapi.annotate import Annotator
from sqapi.request import query_filter as qf
from sqapi.helpers import cli_init, create_parser
from sqapi.media import SQMediaObject
from segment_anything import SamPredictor, sam_model_registry
from skimage import measure
from sys import platform
import torch

DEVICE = "mps" if platform == "darwin" else "cuda" if torch.cuda.is_available() else "cpu"
MODEL_TYPE = "vit_h"
CHECKPOINT_PATH = "models/segment_anything/sam_vit_h_4b8939.pth"


class SegmentAnythingBOT(Annotator):
    def __init__(self, device: str = DEVICE, model_type: str = MODEL_TYPE, model_path: str=CHECKPOINT_PATH, **annotator_args):
        """
        RandoBOT automated annotator
        An example of an automated labelling bot that selects random class labels to assign to points.
        It provides terrible suggestions, however it provides a simple boiler-plate example of how to integrate a
        Machine Learning algorithm for label suggestions.
        """
        super().__init__(**annotator_args)
        print("Loading SAM model...")
        self.model = sam_model_registry[model_type](checkpoint=model_path)
        self.model.to(device=device)
        self.predictor = SamPredictor(self.model)
        self._image_embedding = None

    def process_point(self, point, mediaobj, annotation_set_id):
        point_id = point.get("id")
        x = point.get('x')
        y = point.get('y')
        plgn = point.get("data",{}).get("polygon")
        t = point.get('t')
        is_bbox = self.is_bbox(plgn)

        # if already a polygon, but not a bounding box, leave it out
        if not is_bbox and (isinstance(plgn, list) and len(plgn) >= 3):
            return

        if isinstance(x, float) and isinstance(y, float):  # and not (isinstance(polygon, list) and len(polygon) > 2):
            # check if data has been padded and if not, process it
            if not mediaobj.is_processed:
                orig_image = mediaobj.data()
                image_data = mediaobj.data(cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB))
                self.predictor.set_image(image_data)
                self._image_embedding = self.predictor.get_image_embedding().cpu().numpy()

            points = np.array([[x*mediaobj.width, y*mediaobj.height]])
            lbls = np.array([1 for p in points])

            if is_bbox:  # if there's a bbox, use it to help with the segmentation
                px, py = zip(*plgn)
                bbox = [(min(px)+x)*mediaobj.width, (min(py)+y)*mediaobj.height, (max(px)+x)*mediaobj.width, (max(py)+y)*mediaobj.height]
                box = np.array([int(i) for i in bbox])
            else:     # otherwise asume no bbox
                box = None


            masks, scores, logits = self.predictor.predict(
                box=box,
                point_coords=points,
                point_labels=lbls,
                multimask_output=False
            )
            polygon = self.mask_to_polygon(masks[0])[0]
            # data = dict(pixels=dict(polygon=polygon, row=y*mediaobj.height, col=x*mediaobj.width, width=mediaobj.width, height=mediaobj.height))
            print(f"Number of masks: {len(masks)}, number of vertices in polygon: {len(polygon)}")
            data = dict(pixels=dict(polygon=polygon, width=mediaobj.width, height=mediaobj.height))
            if isinstance(polygon, list) and len(polygon) > 2:
                try:
                    self.sqapi.patch(f"/api/point/{point_id}", json_data=data).execute()
                except Exception as e:
                    print(f"Error: {e}")
                    pass

    def mask_to_polygon(self, mask, tolerance=3.0):
        contours = measure.find_contours(mask, 0.5, positive_orientation='low')
        polygons = []
        for contour in contours:
            contour = np.flip(contour, axis=1)
            simplified_contour = measure.approximate_polygon(contour, tolerance)
            polygons.append(simplified_contour.tolist())

        return polygons

    def is_bbox(self, coordinates):
        if not isinstance(coordinates, list):
            return False
        if len(coordinates) not in [4, 5] or not all((isinstance(i, float) for i in p) for p in coordinates):
            return False  # The LineString must be closed with 4 corners

        # Check
        ind = 0 if coordinates[0][0] == coordinates[1][0] else 1
        matches = []
        for i in range(0, 4):
            matches.append(coordinates[i][ind] == coordinates[(i + 1) % 4][ind])
            ind = 0 if ind == 1 else 1

        return all(matches)


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(SegmentAnythingBOT)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)

    args = parser.parse_args()
    bot = SegmentAnythingBOT(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class, and filter by annotation_set_id
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    # Start the bot in a loop that polls at a defined interval
    bot.start(r)
