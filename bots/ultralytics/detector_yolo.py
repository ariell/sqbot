from ultralytics import YOLO
from sqapi.annotate import Annotator
from sqapi.helpers import create_parser
from sqapi.media import SQMediaObject


class YoloDetector(Annotator):
    def __init__(self, model_path="best.pt", classes=0, conf=0.25, **annotator_args):
        """

        :param model_path: path to the model weights file
        :param classes: classes parameter to feed into .predict() method
        :param conf: conf parameter to feed into .predict() method
        :param annotator_args:
        """
        super().__init__(**annotator_args)
        self.predict_kwargs = dict(classes=classes, conf=conf)
        self.model = YOLO(model_path)

    def detect_points(self, mediaobj: SQMediaObject):
        img = mediaobj.data()  # trigger download to populate width and length
        results = self.model.predict(img, **self.predict_kwargs)    # do the prediction
        points = []
        for result in results:
            boxes = result.boxes.xyxy.cpu().numpy()     # Extract bounding boxes in xyxy format
            scores = result.boxes.conf.cpu().numpy()    # Confidence scores for each box
            class_ids = result.boxes.cls.cpu().numpy()  # Class IDs for each box
            for box, score, class_id in zip(boxes, scores, class_ids):
                p = self.create_annotation_label_point_px(
                    int(class_id), likelihood=float(score), polygon=self.tensor2polygon(box), width=mediaobj.width,
                    height=mediaobj.height)
                points.append(p)
        return points

    def tensor2polygon(self, box):
        # Convert prediction boxes to pixel coordinates
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])

        return [[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]]


if __name__ == '__main__':
    # Running `bot = cli_init(RandoBOT)` would normally do all the steps below and initialise the class,
    # but in this instance we cant to add some extra commandline arguments to decide what annotation_sets to process

    # Get the cli arguments from the Class __init__ function signatures
    parser = create_parser(YoloDetector)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int, required=True)

    args = parser.parse_args()
    bot = YoloDetector(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)

    bot.start(r)

