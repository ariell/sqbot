import datetime
import random
import time
from multiprocessing import Process

import pandas as pd
from flask import Flask, request, jsonify, render_template
from threading import Thread, Semaphore
import uuid
from runbot import run_bot_model, get_cfg
import os
import psutil
import argparse

app = Flask(__name__)
sqapi = None
# DEBUG = False
DEFAULT_CFG = 'server.yaml'


parser = argparse.ArgumentParser(description="SQBOT model server for integration with SQ+")
parser.add_argument('--cfg', default=DEFAULT_CFG, help=f"[OPTIONAL] path to config file (default: '{DEFAULT_CFG}')")
parser.add_argument('--squidle_host', default=None, help=f"[OPTIONAL] override host from cfg file")
args = parser.parse_args()

# get configs
cfg = get_cfg(args.cfg)
if args.squidle_host: cfg['squidle_host'] = args.squidle_host

# Shared dictionary for task statuses
task_status = dict()

system_status = dict(cpu=0, ram=0)

# Create a semaphore for managing concurrency
semaphore = Semaphore(cfg.get('max_concurrent_processes'))


def decode_squidle_payload(token):
    try:
        return sqapi.get(f"/decode_token/{token}?max_age={60*60}").execute().json()
    except Exception as e:
        return dict(status="error", message=str(e))
    # serializer = URLSafeTimedSerializer(credentials.get('squidle_shared_secret'))
    # data = serializer.loads(token, max_age=max_age)
    # return data  # Valid token


def model_task(model_name, task_data, task_id):
    # Initialize task status
    task_status[task_id] = {'status': 'queued', 'model_name': model_name, 'input_data': task_data, 'queued_at': datetime.datetime.utcnow()}
    print(f"[Model Task] Queued {task_id}, awaiting Process slot...")
    with semaphore:  # Acquire semaphore to limit the number of concurrent processes
        if model_name not in cfg.get('models'):
            return jsonify({'error': 'Model not found'}), 404
        assert task_data.get("resource") in ["annotation_set"], \
            "Invalid resource type. Currently only supports annotation_sets."
        annotation_set_id = task_data.get('id')
        annotation_set_params = {'annotation_set_id': annotation_set_id}

        # Update task status to 'running'
        task_status[task_id].update({'status': 'running', 'started_at': datetime.datetime.utcnow()})
        print(f"[Model Task] Starting Process {task_id} with model {model_name} and input {annotation_set_params}")

        botcfg = cfg.get("models").get(model_name)
        botparams = botcfg.get("bot_params", {}).copy()
        botparams['host'] = sqapi.host
        botparams['api_key'] = sqapi.api_key
        # Process the input data using the model
        try:
            if botcfg.get('run_as_process'):   # run as a separate process
                process = Process(target=run_bot_model, args=(botcfg.get('cfg'), annotation_set_params), kwargs=botparams)
                process.start()
                process.join()  # Wait for the process to complete
            else:  # run in this task thread
                run_bot_model(botcfg.get('cfg'), annotation_set_params, **botparams)
            status = "complete"
            message = ""
        except Exception as e:
            status = "error"
            message = f"{e}"

        # Task is complete
        # Append the data to the CSV file
        task_data = task_status.pop(task_id)
        task_data.update({'status': status, 'completed_at': datetime.datetime.utcnow(), 'message': message})
        df = pd.json_normalize(task_data)  # flatten for csv
        logfile = cfg.get('logfile_tasks')
        df.to_csv(logfile, mode='a', header=not os.path.isfile(logfile), index=False)  # add header if new file
        print(f"[Model Task] Process {task_id} completed")


def render_response(data, template=None, **kwargs):
    template = template or request.args.get("template", None) or request.headers.get("X-template", None)
    if template and request.accept_mimetypes.accept_html:
        # Render the HTML template with the data
        return render_template(template, data=data, uuid=uuid.uuid4(), **kwargs)
    else:
        # Return JSON data
        return jsonify(data), 200

@app.route('/model/<model_name>/<token>', methods=['POST'])
def run_model(model_name, token):
    task_data = decode_squidle_payload(token)

    # Create a unique task ID using UUID
    task_id = str(uuid.uuid4())

    # Put the task in the queue
    t = Thread(target=model_task, args=(model_name, task_data, task_id), daemon=True)
    t.start()

    return jsonify({'task_id': task_id}), 200

@app.route('/model', methods=['GET'], defaults={'model_name': None})
@app.route('/model/<model_name>', methods=['GET'])
def list_models(model_name=None):
    if model_name is None:
        # data = cfg.get("models").copy()
        data = {k: get_cfg(v.get("cfg")) for k, v in cfg.get("models").items()}
    else:
        if model_name not in cfg.get('models'):
            return jsonify({'error': 'Model not found'}), 404
        data = cfg.get("models").get(model_name).copy()
        data['cfg_data'] = get_cfg(data.get("cfg"))
    return render_response(data)

@app.route('/dataset/<token>')
def preview_dataset(token):
    params = decode_squidle_payload(token)
    if params.get("status") == "error":
        print(params)
        return jsonify(params), 400
    data = sqapi.get(f"/api/{params.get('resource')}/{params.get('id')}").execute().json()
    return render_response(data, params=params, template=f"datasets/{params.get('resource')}.html")

@app.route('/status', methods=['GET'], defaults={'task_id':None})
@app.route('/status/<task_id>', methods=['GET'])
def queue_status(task_id=None):
    if task_id is None:
        data = {
            'datetime': datetime.datetime.utcnow(),
            'cpu':  system_status.get('cpu'),
            'ram': system_status.get('ram'),
            'queued_tasks': {task: status for task, status in task_status.items() if status.get('status') == 'queued'},
            'running_tasks': {task: status for task, status in task_status.items() if status.get('status') == 'running'}
        }
    else:
        data = task_status.get(task_id)
    return render_response(data=data)

@app.route('/<dataset_token>')
def index(dataset_token):
    return render_template('layout.html', dataset_token=dataset_token, sqhost=sqapi.host, squser=sqapi.current_user, cfg=cfg)


def update_system_status():
    while True:
        system_status['cpu'] = psutil.cpu_percent(cfg.get("status_interval", 1))  # blocking, interval (seconds)
        system_status['ram'] = psutil.virtual_memory().percent


def run_server(controller=None):
    global sqapi
    if controller is None:
        from sqapi.api import SQAPI
        sqapi = SQAPI(host=cfg.get('squidle_host'))
    else:
        sqapi = controller.sqapi

    Thread(target=update_system_status, daemon=True).start()  # start system update process

    # Use threaded=True to handle multiple requests in Flask and avoid blocking issues
    app.run(debug=False, use_reloader=False, threaded=True, port=cfg.get('port'), host='0.0.0.0')


if __name__ == '__main__':
    run_server()

