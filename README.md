# SQBOT (based on [sqapi](https://bitbucket.org/ariell/sqapi))
A repository to help plug machine learning algorithms into Squidle+ ([squidle.org](https://squidle.org))
using the [`sqapi`](https://bitbucket.org/ariell/sqapi) software library and the 
[SQUIDLE+ API](https://squidle.org/api/help?template=api_help_page.html)

## Models and Bots
This repository contains `models` and `bots`. The `models` directory contains a library of pretrained machine learning 
models (or "checkpoints"), as well as the parameters to run them, contained in a `runbot.yaml` configuration file.
The `bots` directory contains the software modules, libraries and dependencies required to run the various `models`, 
with specific Machine Learning architecture, like `keras` or `tensorflow`. The `models` and `bots` can be used 
for detection tasks, classification tasks, segmentation, active learning or any sort of interaction that can be 
facilitated through the API. Algorithms run as users in Squidle+ using an API key and data can be shared with them 
using the same sharing framework for sharing and collaborating with other users. 

## Installation
Each `bot` is a python module (containing an `__init__.py` file) and contains a `requirements.txt` file that includes
that module's specific dependencies. It is recommended that you create virtual environments. To create  
a separate virtual environment per `bot`:
```shell
cd bots/name_of_bot
virtualenv -p python3 env
source env/bin/activate  # activate the virtual environment
pip install -r requirements.txt
```

Alternatively, if you want to install them all in the same virtual environment (this can lead to dependency version 
issues, but usually tends to work), simply create a virtual environment in the sqbot root directory and install all the 
requirements files:
```shell
# from the sqbot root directory
virtualenv -p python3 env
source env/bin/activate  # activate the virtual environment
ls bots/*/requirements.txt | xargs -I {} pip install -r {}  # install requirements for all bots
```

## Anatomy of a **bot**
The `bots` directory is a python module containing submodules that contain all the software required to run the 
`models`. The structure for the bots are as follows:

```
bots/
├─ bot1/                        # directory containing software / architecture for running specific models
│  ├─ utils/                    # optional directories containing useful functions and code for the bot
│  │  ├─ useful_functions.py    # flexibly define utilility functions that can be used by the core algorithms
│  │  └─ :  
│  ├─ classifier.py             # code to run automated labelling models
│  ├─ detector.py               # code to run object detection models 
│  ├─ requirements.txt          # python pip dependencies
│  └─ README.md                 # Some useful documentation on this bot
├─ bot2/                        # directory containing software / architecture for running specific models
│  ├─ auto_segment_points.py    # code that could be used for automated polygon creation
│  ├─ active_learning.py        # code used to run interactive active learning processes
│  ├─ requirements.txt          # python pip dependencies
│  └─ README.md                 # Some useful documentation on this bot
├─ :
└─ :
```
See the actual examples in the [bots](bots) directory for more information.

## Anatomy of the **models** library
```
bots/
├─ contributor1/                # top level grouping of models by contributor 
│  ├─ model1name/               # model name
│  │  ├─ label_map.json         # a file containing a mapping between SQ+ labels and categories of the model
│  │  ├─ model_file.ckpt        # the model file or checkpoint
│  │  └─ runbot.yaml            # the configuration file outlining how to run this model including which bot to use and what parameters
│  ├─ model2name/               # model name
│  │  ├─ label_map.json         # a file containing a mapping between SQ+ labels and categories of the model
│  │  ├─ model_file.mdl         # the model file or checkpoint
│  │  └─ runbot.yaml            # the configuration file outlining how to run this model including which bot to use and what parameters
│  └─ :
├─ contributor2/
│  └─ :
└─ :
```
See the actual examples in the [models](models) directory for more information.

## **Bot** & **Model** deployment
To run a model/bot directly, see the section below, but in general, deployment is handled through a model server which 
provides a UI to run and configure models and instantiates, queues and runs model processes. The list of models to run
is handled by a server config file, with the default being [`server.yaml`](server.yaml). This sets up which models to 
run and how. To set up the server:

```bash
pip install -r server_requirements.txt
```

To run the server:
```bash
python server.py
```
Or for options, check out:
```bash
python server.py --help
```
For a custom deployment, you can create your own `server.yaml` file and pass it in as a `--cfg` argument when running `server.py`

## Running a **bot** / **model** directly
Each `bot` can be run directly from the command line interface and all parameters can be passed in through the CLI.
For example, to see help on what parameters are required for the demo bots, simply execute the following from the command line:
```shell
# from within the appropriate virtual environment
python -m bots.demo.classifier --help
```
That will output:
```
usage: classifier.py [-h] [--host HOST] [--api_key API_KEY] [--annotator_info ANNOTATOR_INFO] [--prob_thresh PROB_THRESH] [--poll_delay POLL_DELAY] [--label_map_file LABEL_MAP_FILE] [--verbosity VERBOSITY] [--email_results] [--non_supplementary]
                     [--label_map_filters LABEL_MAP_FILTERS] [--annotation_set_id ANNOTATION_SET_ID]

RandoBOT automated annotator
An example of an automated labelling bot that selects random class labels to assign to points.
It provides terrible suggestions, however it provides a simple boiler-plate example of how to integrate a
Machine Learning algorithm for label suggestions.

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           the Squidle+ instance hostname
                        type: str  | default: https://squidle.org
  --api_key API_KEY     the API key for the user on that `host`. If omitted, you'll be prompted to log in.
                        type: str  | default: None
  --annotator_info ANNOTATOR_INFO
                        used for the name of the annotation_set, include version info, defaults to ClassName
                        type: str  | default: None
  --prob_thresh PROB_THRESH
                        probability threshold for submitted labels, only submitted if p > prob_thresh
                        type: float  | default: 0.5
  --poll_delay POLL_DELAY
                        the poll delay for running the loop (in seconds). To run once, set poll_delay = -1 (default)
                        type: int  | default: -1
  --label_map_file LABEL_MAP_FILE
                        path to a local file that contains the label mappings
                        type: str  | default: None
  --verbosity VERBOSITY
                        the verbosity of the output (0,1,2,3)
                        type: int  | default: 2
  --email_results       flag to optionally send an email upon completion
                        type: bool  | default: None
  --non_supplementary   flag to optionally label directly without creating a supplementary set (with suggestions)
                        type: bool  | default: False
  --label_map_filters LABEL_MAP_FILTERS
                        
                        default: None
  --annotation_set_id ANNOTATION_SET_ID
                        Process specific annotation_set
```
So to run this example, you may want to run something like:
```shell
python -m bots.demo.classifier --label_map_file models/demo/classifier/random_label_map_keys.json --annotation_set_id ${ID}
```

However, the real, non-demo bots typically have many more parameters and running them each time from the CLI can get a
little clunky. So to streamline the running and deployment of models, there is a file in the sqbot root directory called
[runbot.py](runbot.py). It works in conjunction with the [runbot.yaml](models/demo/classifier/runbot.yaml) files that 
are associated with each model. These config files tell `runbot.py` which bot to use and what parameters to pass in for
running the models.

So to run a model, you'd execute something like:
```shell
python runbot.py models/demo/classifier/runbot.yaml  --annotation_set_id ${ID}
```

Note, `runbot.py` also provides different ways to define filters on annotation set on which to run the models/bots.
For more on this, see the `runbot.py` help by executing:
```shell
python runbot.py --help
```


## Creating your own **bot**
Follow the examples in the existing `bot` directories. 
A good place to start is by looking at the `demo_bot` example. It provides a classifier and a detector
which just generate random garbage labels, however, they provide very simple boiler-plate examples that can be fleshed
out to make real classifiers and detectors.

### 0. Create a new **bot** directory
Create a new directory that replicates the structure specified in the description above.

### 1. Create a bot class
In this example, we'll create an automated classifier that suggests random label suggestions for an `annotation_set`. 
It uses the `Annotator` class from the `sqapi.annotate` module, which does most of the heavy-lifting. The implementation
of the classifier is relatively straight forward.
In your `bot` directory, in the file named `classifier.py`:

```python
# classifier.py
import random
from sqapi.annotate import Annotator
from sqapi.helpers import create_parser
from sqapi.media import SQMediaObject


class RandoBOT(Annotator):
    """
    An example of an automated labelling bot that selects random class labels to assign to points.
    It provides terrible suggestions, however it provides a simple boiler-plate example of how to integrate a
    Machine Learning algorithm for label suggestions.
    """       
    def classify_point(self, mediaobj: SQMediaObject, x, y, t):
       # This method is used to generate a point prediction
       # TODO: use the mediaobj, x and y point position to generate a real classifier_code and prob
       classifier_code = random.randint(0,3)  # get a random code (0-3) to match with label_map_file
       prob = round(random.random(), 2)  # generate a random probability
       return classifier_code, prob


if __name__ == '__main__':
    # Get the cli arguments from the Class __init__ function signatures 
    # (i.e. the arguments of RandoBOT, and the base Annotator class and documentation blocks)
    parser = create_parser(RandoBOT)

    # Add some additional custom cli args not related to the model
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int, required=True)

    args = parser.parse_args()
    bot = RandoBOT(**vars(args))

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = bot.sqapi.get("/api/annotation_set").filter("id", "eq", args.annotation_set_id)
    
    # Start the bot
    bot.start(r)
```

The `create_parser` method is a convenience tool that allows you to build a command line parser from the  
signature of the constructor (`__init__` methods) for the `RandoBot` class and all inherited base classes (in this case, the `Annotator` class).

This allows you to pass arguments from the command line, and helps to show how to use it. Note it is a python module,
so it needs to be run with the `-m` argument as follows:
```shell
# from the command line, run:
python -m bots.demo.classifier --help
```

It will show you all the required parameters from the base class `Annotator` as well as any extra arguments added to the 
`__init__` method of the `RandoBot` class. Parameter descriptions come from the comment block in the relevant classes.

### 2. Create a Label Mapper File
Before you run it, you also need to create a label map file to pass into the `--label_map_file` argument. 
This maps the outputs from your classifier to real class labels in the system.

Define a `label_map.json` file (typically done in the `models` directory, 
with the following content:
```json
[
  [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"214344"}}],
  [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"1839"}}],
  [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"82001000"}}],
  null 
]
```
The example above, shows a label_map file for a classifier that outputs an integer value classifier code, where `0-2` 
would be a Label class that uses the vocab_element to lookup a Label across the Label Schemes. `3` would output nothing, 
which won't be submitted.

Note: if your classifier outputs a string code instead of an integer, you can define a similar 
mapping file using a dict lookup, like:
```json
{
  "ECK": [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"214344"}}],
  "ASC": [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"1839"}}],
  "SUB": [{"name":"vocab_elements","op":"any","val":{"name":"key","op":"eq","val":"82001000"}}]
}
```
That will attempt to map the classifier outputs `ECK`, `ASC` and `SUB` to a Label in SQ+.

### 3. Run your **bot**
Now you're ready to run your classifier `RandoBot`, by simply executing this from the command line:
```shell
# bash
# See help to show all arguments
python -m bots.demo.classifier --help
# Run automated labeler algorithm with selected arguments
python -m bots.demo.classifier --host https://staging.squidle.org --label_map_file models/demo/classifier/random_label_map_keys.json --prob_thresh 0.5 --email_results --annotation_set_id <ID>
```

This will prompt you for a annotation_set id and attempt to provide automated suggestions on the labels it contains 
using random class allocations and probabilities. It will only submit suggestions with a probability > 0.5 and 
it will run once (as defined by the `--poll_delay=-1` parameter) 
and it will send an email to the owner the annotation_set once complete.

Now all that's left is for you to make the labels and probabilities real, and bob's your uncle,
you've made an automated classifier.

### 4. Defining a **runbot.yaml** config file for your model
While running the bots and models from the command line is possible, and is certainly useful for testing, it can be cumbersome 
for deploying multiple models. To streamline model deployment, each model should have an associated `runbot.yaml` file 
that contains all the parameters on how to run the model and which bot to use. For example, in our demo above, we can 
define the following `runbot.yaml` file:
```yaml
name: Demo-Random-Point-Classifier
description: Not real. Bare minimum demo classifier that assigns a random supplementary label (boiler plate example).
author: Ariell Friedman (Greybits Engineering)  # Information about the creator / author
bot: bots.demo.classifier.RandoBOT              # which bot class to use to run the model
model_url: null                                 # this bot just randomly picks labels, so it doesn't actually have a model
model_file: null                                # if this had a model, this is where it would live (for auto-download)
label_map_file: random_label_map_keys.json      # the path to the label_map file (relative to this yaml file)
bot_parameters:                                 # parameters passed directly to the bot class, in this case RandoBOT
  prob_thresh: 0.5                              # probability threshold for submitted labels, only submitted if p > prob_thresh
  non_supplementary: false                      # creating a supplementary set (with suggestions) instead of label directly
  email_results: false                          # whether to email annotation_set owner upon completion
#  verbosity: 2  # 1,2,3                        # level of output on cli during execution
metadata:                                       # metadata to attach to the supplementary annotation_set
  version: 0.1                                  # ML model version
  architecture: null                            # ML bot architecture
properties:                                     # information about what this model/bot does
  create_points: false                          # create new points (i.e detector)
  create_bboxes: false                          # creates bounding boxes
  create_polygons: false                        # creates polygons
  update_annotations: true                      # updates annotation labels
  active_learning: false                        # implements active learning
  is_full_bio_score: false                      # scoring resolution, using the full label scheme, or targeted
  is_real_science: false                        # is intended for real science, or just experimental
```

Once this is defined, the bot can be run with the model simply by passing a `runbot.yaml` file into the `runbot.py` 
script, as follows:
```bash
python runbot.py models/demo/classifier/runbot.yaml
```

Or to deploy on the bot server, add it to a `server.yaml` file.