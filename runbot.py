import argparse
import yaml
import importlib
from sqapi.request import query_filter as qf
import os


def create_parser():
    parser = argparse.ArgumentParser(description="A script for running bots on specific models using the runbot.yaml setup")

    # Add some additional custom cli args not related to the model
    parser.add_argument('cfg', help="Path to a runbot.yaml config file which contains all the parameters required to run a model", type=str)
    parser.add_argument('--annotation_set_id', help="Process specific annotation_set", type=int)
    parser.add_argument('--user_group_id', help="Process all annotation_sets contained in a specific user_group", type=int)
    parser.add_argument('--affiliation_group_id', help="Process all annotation_sets contained in a specific Affiliation (NOTE: this may yeild unexpected results due to dataset privacy restrictions)",type=int)
    parser.add_argument('--after_date', help="Process all annotation_sets after a date YYYY-MM-DD", type=str)
    parser.add_argument('--media_count_max', help="Process annotation_sets that have less than a specific number of media objects", type=int)
    parser.add_argument('--api_key', default=None, required=False, help="the API key for the user on that `host`. If omitted (recommended), you'll be prompted to log in.", type=str)
    parser.add_argument('--host', default=None, required=False, help="SQ+ host", type=str)

    return parser


def get_cfg(cfg_file):
    # Load YAML data from a file into a Python dictionary
    with open(cfg_file, 'r') as file:
        return yaml.safe_load(file)


def get_bot(cfg, modle_path, **kw):
    # Build up bot params
    bot_kw = cfg.get("bot_parameters", {})
    bot_kw.update(kw)
    annotator_info = dict(
        name=cfg.get("name"),
        description=cfg.get("description"),
        is_full_bio_score=cfg.get("properties", {}).get("is_full_bio_score", False),
        is_real_science=cfg.get("properties", {}).get("is_real_science", False))
    metadata = dict(
        model=cfg.get("model_file"),
        bot=cfg.get('bot'),
        **cfg.get("metadata", {}))
    bot_kw['metadata'] = metadata
    bot_kw['annotator_info'] = annotator_info

    # Resolve model_path
    if cfg.get("model_file") is not None:
        bot_kw['model_path'] = os.path.join(modle_path, cfg.get("model_file"))
        if not os.path.isfile(bot_kw['model_path']):
            os.system(f"aws s3 cp {cfg['model_url']} {bot_kw['model_path']}")

    # Resolve label_map_file
    if cfg.get("label_map_file") is not None:
        bot_kw['label_map_file'] = os.path.join(modle_path, cfg.get("label_map_file"))

    # Split the class path into module and class name
    module_name, class_name = cfg.get('bot').rsplit('.', 1)

    # Dynamically get the class from the module
    BotClass = getattr(importlib.import_module(module_name), class_name)

    # instantiate bot
    # print(bot_kw)
    return BotClass(**bot_kw)


def get_annotation_set_query(sqapi, params):
    has_sufficient_filter = False

    # Initialise annotation_set request using sqapi instance in Annotator class
    r = sqapi.get("/api/annotation_set")

    # Filter annotation sets based on ID
    if params.get("annotation_set_id"):
        r.filter("id", "eq", params.get("annotation_set_id"))
        has_sufficient_filter = True
    else:
        # Only return annotation_sets that do not already have suggestions from this user
        r.filter_not(qf("children", "any", val=qf("user_id", "eq", sqapi.current_user.get("id"))))

    # Constrain date ranges to annotation_sets ceated after a specific date
    if params.get("after_date"):
        r.filter("created_at", "gt", params.get("after_date"))

    # Filter annotation_sets based on a user group
    if params.get("user_group_id"):
        r.filter("usergroups", "any", val=qf("id", "eq", params.get("user_group_id")))
        has_sufficient_filter = True

    if params.get("affiliation_group_id"):
        r.filter("user", "has", val=qf("affiliations_usergroups", "any", val=qf("group_id", "eq", params.get("affiliation_group_id"))))
        has_sufficient_filter = True

    if params.get("media_count_max"):
        r.filter("media_count", "lte", params.get("media_count_max"))

    assert has_sufficient_filter, \
        "The annotation_set filter is not sufficiently constrained. " \
        "Please check your input arguments and set some criteria."

    return r


def run_bot_model(botcfg_file, annotation_set_params, host=None, api_key=None, poll=False, **kw):
    cfg = get_cfg(botcfg_file)
    bot = get_bot(cfg, os.path.dirname(botcfg_file), host=host, api_key=api_key, **kw)
    annotation_set_parameters = cfg.get('annotation_set_parameters', {})
    annotation_set_parameters.update(annotation_set_params)
    r = get_annotation_set_query(bot.sqapi, annotation_set_parameters)
    if poll:
        bot.start(r)   # start process loop with poll_delay param
    else:
        bot.run(r)      # only run once
    # time.sleep(random.randint(30, 60))


if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()
    # cfg = get_cfg(args.cfg)

    # Configure bot params from cfg file and override with CLI args if provided
    annotation_set_parameters = {k: v for k, v in vars(args).items() if v is not None}

    run_bot_model(args.cfg, annotation_set_parameters, host=args.host, api_key=args.api_key, poll=True)
